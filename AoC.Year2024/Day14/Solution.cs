namespace AoC.Year2024.Day14;

public partial class Solution : Day<(List<Solution.Robo> robots, Point size)>
{
    [GeneratedRegex(@"p=(?<pX>-?\d+),(?<pY>-?\d+) v=(?<vX>-?\d+),(?<vY>-?\d+)")]
    private static partial Regex RoboRegex();

    public record Robo(Point Position, Point Velocity)
    {
        public Robo(Match match) : this(
            new Point(match.Groups["pX"].Value.ToLong(), match.Groups["pY"].Value.ToLong()),
            new Point(match.Groups["vX"].Value.ToLong(), match.Groups["vY"].Value.ToLong())
        )
        {
        }
    }

    protected override object DoPart1((List<Robo> robots, Point size) input) =>
        GetQuadrants(TickRobots(input.robots, input.size, 100), input.size)
            .Aggregate(1, (acc, q) => acc * q.Count);

    protected override object DoPart2((List<Robo> robots, Point size) input)
    {
        ProgressReporter?.SetSteps(10000);
        return ParallelEnumerable.Range(0, 10000)
            .Select(i =>
            {
                ProgressReporter?.Step();
                return FindNeighbours(TickRobots(input.robots, input.size, i), i);
            })
            .ToList()
            .MaxBy(i => i.neighbours)
            .index;
    }

    private static (int neighbours, int index) FindNeighbours(List<Robo> input, int index) =>
        (input
            .Sum(robo => robo.Position
                .ToDirectionalNeighbours()
                .Count(potentialNeighbour => input.Any(r => r.Position == potentialNeighbour))), index);

    private static List<List<Robo>> GetQuadrants(List<Robo> input, Point size) =>
    [
        GetRobotsInQuadrant(input, new Point(0, 0), new Point(size.X / 2, size.Y / 2)),
        GetRobotsInQuadrant(input, new Point(size.X / 2 + 1, 0), size with { Y = size.Y / 2 }),

        GetRobotsInQuadrant(input, new Point(0, size.Y / 2 + 1), size with { X = size.X / 2 }),
        GetRobotsInQuadrant(input, new Point(size.X / 2 + 1, size.Y / 2 + 1), new Point(size.X, size.Y))
    ];

    private static List<Robo> GetRobotsInQuadrant(List<Robo> input, Point min, Point max) =>
        input.Where(robo => robo.Position.X >= min.X && robo.Position.X < max.X && robo.Position.Y >= min.Y &&
                            robo.Position.Y < max.Y).ToList();

    private static List<Robo> TickRobots(List<Robo> input, Point size, int amount = 1) =>
        input.Select(robo => robo with
        {
            Position = new Point(
                ((robo.Position.X + robo.Velocity.X * amount) % size.X + size.X) % size.X,
                ((robo.Position.Y + robo.Velocity.Y * amount) % size.Y + size.Y) % size.Y)
        }).ToList();

    protected override (List<Robo> robots, Point size) ParseInputPart1(string input)
    {
        var robots = RoboRegex()
            .Matches(input)
            .Select(m => new Robo(m))
            .ToList();

        return (robots, robots.Count < 15 ? new Point(11, 7) : new Point(101, 103));
    }
}