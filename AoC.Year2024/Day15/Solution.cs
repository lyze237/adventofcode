namespace AoC.Year2024.Day15;

public class Solution : Day<(char[][] map, Direction[] directions)>
{
    protected override object DoPart1((char[][] map, Direction[] directions) input)
    {
        foreach (var direction in input.directions)
        {
            var mapState = input.map.CloneArray();
            
            if (TryMoveObject(mapState, FindRobot(mapState), '@', direction))
                input.map = mapState;
        }

        return CountBoxes(input.map);
    }
    
    protected override object DoPart2((char[][] map, Direction[] directions) input) => 
        DoPart1(input);

    private static long CountBoxes(char[][] map)
    {
        long boxes = 0;
        for (var y = 0; y < map.Length; y++)
            for (var x = 0; x < map[y].Length; x++)
                if (map[y][x] is 'O' or '[')
                    boxes += y * 100 + x;
        return boxes;
    }

    private static Point FindRobot(char[][] map)
    {
        for (var y = 0; y < map.Length; y++)
            for (var x = 0; x < map[y].Length; x++)
                if (map[y][x] == '@')
                    return new Point(x, y);
        
        throw new ArgumentException("Robot not found");
    }


    private static bool TryMoveObject(char[][] map, Point currentPosition, char objectSymbol, Direction moveDirection)
    {
        var targetPosition = currentPosition.Move(moveDirection);
        var targetObject = map.Get(targetPosition);

        switch (targetObject)
        {
            case '.':
                return UpdateMap(map, currentPosition, targetPosition, objectSymbol);
            case '#':
                return false;
            case 'O' when TryMoveObject(map, targetPosition, targetObject, moveDirection):
                return UpdateMap(map, currentPosition, targetPosition, objectSymbol);
        }

        if (moveDirection.IsHorizontal() && TryMoveObject(map, targetPosition, targetObject, moveDirection))
            return UpdateMap(map, currentPosition, targetPosition, objectSymbol);

        if (!moveDirection.IsVertical()) 
            return false;

        return targetObject switch
        {
            '[' when TryMoveObject(map, targetPosition, targetObject, moveDirection) && TryMoveObject(map, targetPosition.Move(Direction.Right), ']', moveDirection) =>
                UpdateMap(map, currentPosition, targetPosition, objectSymbol),
            ']' when TryMoveObject(map, targetPosition, targetObject, moveDirection) && TryMoveObject(map, targetPosition.Move(Direction.Left), '[', moveDirection) =>
                UpdateMap(map, currentPosition, targetPosition, objectSymbol),
            _ => false
        };
    }

    private static bool UpdateMap(char[][] map, Point oldPosition, Point newPosition, char symbol)
    {
        map.Set(oldPosition, '.');
        map.Set(newPosition, symbol);
        return true;
    }

    protected override (char[][] map, Direction[] directions) ParseInputPart1(string input)
    {
        var parts = input.Split("\n\n");
        var map = parts[0].Split("\n").Select(line => line.ToCharArray()).ToArray();

        var directions = parts[1]
            .Where(lines => lines != '\n')
            .Select(direction => direction switch
            {
                '<' => Direction.Left,
                '>' => Direction.Right,
                '^' => Direction.Up,
                'v' => Direction.Down,
                _ => throw new Exception()
            }).ToArray();

        return (map, directions);
    }
    
    protected override (char[][] map, Direction[] directions) ParseInputPart2(string input)
    {
        var (map, directions) = ParseInputPart1(input);
        
        var adjustedMap = new char[map.Length][];
        
        for (var y = 0; y < map.Length; y++)
        {
            adjustedMap[y] = new char[map[y].Length * 2];
            
            for (var x = 0; x < map[y].Length; x++)
            {
                adjustedMap[y][x * 2] = map[y][x] switch
                {
                    'O' => '[',
                    _ => map[y][x]
                };

                adjustedMap[y][x * 2 + 1] = map[y][x] switch
                {
                    'O' => ']',
                    '@' => '.',
                    _ => map[y][x]
                };
            }
        }

        return (adjustedMap, directions);
    }
}