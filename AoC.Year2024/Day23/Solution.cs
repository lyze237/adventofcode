using GraphLibrary;
using GraphLibrary.Graphs.GraphsFast;

namespace AoC.Year2024.Day23;

public class Solution : Day<(Dictionary<string, int> mapping, (int from, int to)[] edges)>
{
    protected override object DoPart1((Dictionary<string, int> mapping, (int from, int to)[] edges) input)
    {
        var graphWithT = new GraphFastUndirected();
        var graphWithoutT = new GraphFastUndirected();

        foreach (var (from, to) in input.edges)
        {
            AddToGraph(from, to, graphWithT);
            
            var fromStr = GetMapping(from, input.mapping);
            var toStr = GetMapping(to, input.mapping);
            
            if (!fromStr.StartsWith('t') && !toStr.StartsWith('t'))
                AddToGraph(from, to, graphWithoutT);
        }

        var cluster = new Clustering();
        var trianglesWithT = cluster.Triangles(graphWithT);
        var trianglesWithoutT = cluster.Triangles(graphWithoutT);

        return (trianglesWithT.Values.Sum() - trianglesWithoutT.Values.Sum()) / 3;
    }
    
    protected override object DoPart2((Dictionary<string, int> mapping, (int from, int to)[] edges) input)
    {
        var graph = new GraphFastUndirected();

        foreach (var (from, to) in input.edges)
            AddToGraph(from, to, graph);

        var nodes = FindLargestCliques(graph)
            .Select(node => GetMapping(node, input.mapping))
            .Order();

        return string.Join(",", nodes);
    }

    private static List<int> FindLargestCliques(GraphFastUndirected graph)
    {
        var cliques = graph.Network.Keys.Select(node => new List<int> { node }).ToList();

        while (true)
        {
            var grownCliques = new List<List<int>>();
            foreach (var clique in cliques)
                grownCliques.AddRange(GetCommonNeighbors(clique, graph).Select(neighbor => new List<int>(clique) { neighbor }));

            cliques = DeduplicateCliques(grownCliques);
            if (cliques.Count == 1)
                return cliques[0];

            if (grownCliques.Count == 0)
                return cliques.OrderByDescending(c => c.Count).First();
        }
    }

    private static List<int> GetCommonNeighbors(List<int> clique, GraphFastUndirected graph)
    {
        return graph.Network[clique[0]]
            .Where(neighbor => clique.All(node => graph.Network[node].Contains(neighbor)))
            .ToList();
    }

    private static List<List<int>> DeduplicateCliques(IEnumerable<List<int>> cliques)
    {
        var deduplicated = new HashSet<int[]>(new ArrayEqualityComparer<int>());
        var result = new List<List<int>>();

        foreach (var clique in cliques)
        {
            var sortedClique = clique.OrderBy(n => n).ToArray();

            if (deduplicated.Add(sortedClique))
                result.Add(sortedClique.ToList());
        }

        return result;
    } 
    
    private static void AddToGraph(int from, int to, GraphFastUndirected graph)
    {
        if (!graph.Nodes.Contains(from))
            graph.NodeAdd(from);
        if (!graph.Nodes.Contains(to))
            graph.NodeAdd(to);

        graph.EdgeAdd(from, to);
    }

    private static string GetMapping(int value, Dictionary<string, int> mapping)
    {
        foreach (var (key, val) in mapping)
            if (val == value)
                return key;

        throw new ArgumentException($"Mapping {value} not found");
    }

    protected override (Dictionary<string, int> mapping, (int from, int to)[] edges) ParseInputPart1(string input)
    {
        var mapping = new Dictionary<string, int>();
        var result = new List<(int from, int to)>();

        foreach (var line in input.Split("\n"))
        {
            var (fromStr, toStr) = line.Split("-");

            if (!mapping.TryGetValue(fromStr!, out var from))
                mapping[fromStr!] = from = mapping.Count;

            if (!mapping.TryGetValue(toStr!, out var to))
                mapping[toStr!] = to = mapping.Count;

            result.Add((Math.Min(from, to), Math.Max(from, to)));
        }

        return (mapping, result.Distinct().ToArray());
    }
}