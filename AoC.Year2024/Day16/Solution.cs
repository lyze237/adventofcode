namespace AoC.Year2024.Day16;

public class Solution : Day<char[][]>
{
    protected override object DoPart1(char[][] input) =>
        SolveMaze(input).bestScore;

    protected override object DoPart2(char[][] input) =>
        SolveMaze(input).bestSeats;

    private static (int bestScore, int bestSeats) SolveMaze(char[][] input)
    {
        var startPoint = FindSymbolOnMap(input, 'S');
        var endPoint = FindSymbolOnMap(input, 'E');

        return BreadthFirstSearch(startPoint, endPoint, input);
    }

    private static (int bestScore, int bestSeats) BreadthFirstSearch(Point start, Point end, char[][] input)
    {
        var visited = new Dictionary<(Point point, Direction direction), int>();

        var queue = new PriorityQueue<(Point point, int score, Direction direction, HashSet<Point> seats), int>();
        queue.Enqueue((start, 0, Direction.Right, []), 0);

        (int score, HashSet<Point> seats) best = (score: int.MaxValue, [start]);

        while (queue.Count > 0)
        {
            var (point, score, direction, seats) = queue.Dequeue();

            if (point == end)
            {
                best.score = score;
                best.seats.UnionWith(seats);
            }

            if (score > best.score)
                break;

            if (visited.GetValueOrDefault((point, direction), int.MaxValue) < score)
                continue;

            visited.TryAdd((point, direction), score);

            foreach (var newDirection in new[] { direction, direction.RotateLeft90(), direction.RotateRight90() })
            {
                var newPoint = point.Move(newDirection);
                if (input.Get(newPoint) == '#')
                    continue;

                var newSeats = new HashSet<Point>(seats) { newPoint };

                var newScore = newDirection == direction ? score + 1 : score + 1001;
                queue.Enqueue((newPoint, newScore, newDirection, newSeats), newScore);
            }
        }

        return (best.score, best.seats.Count);
    }

    private static Point FindSymbolOnMap(char[][] input, char symbol)
    {
        for (var y = 0; y < input.Length; y++)
        {
            for (var x = 0; x < input[y].Length; x++)
            {
                if (input[y][x] == symbol)
                    return new Point(x, y);
            }
        }

        throw new ArgumentException("Symbol not found on map");
    }

    protected override char[][] ParseInputPart1(string input) =>
        input.Split("\n").Select(line => line.ToCharArray()).ToArray();
}