namespace AoC.Year2024.Day20;

public class Solution : Day<char[][]>
{
    private record Cheat(Point Start, Point End)
    {
        public bool IsValid(Dictionary<Point, int> costs, int toSave = 100) =>
            costs[Start] + Start.ManhattanDistance(End) <= costs[End] - toSave;
    }

    protected override object DoPart1(char[][] input) =>
        CalculateCheats(input, 2);

    protected override object DoPart2(char[][] input) =>
        CalculateCheats(input, 20);

    private static int CalculateCheats(char[][] input, int picoseconds)
    {
        var start = GetPoint(input, 'S');
        var end = GetPoint(input, 'E');

        var costs = BreadthFirstSearch(start, end, input);

        return costs.Keys
            .AsParallel()
            .SelectMany(point => GetCheatsForPoint(point, picoseconds, input))
            .Count(cheat => cheat.IsValid(costs));
    }

    private static List<Cheat> GetCheatsForPoint(Point start, int picoseconds, char[][] input)
    {
        var cheats = new List<Cheat>();

        foreach (var (x, y, _) in input.Iterate())
        {
            var point = new Point(x, y);
            if (input.Get(point) == '#')
                continue;

            if (start.ManhattanDistance(point) > picoseconds)
                continue;

            cheats.Add(new Cheat(start, point));
        }

        return cheats;
    }

    private static Dictionary<Point, int> BreadthFirstSearch(Point start, Point end, char[][] input)
    {
        var queue = new Queue<Point>();
        queue.Enqueue(start);

        var costs = new Dictionary<Point, int> { [start] = 0 };

        while (queue.Count > 0)
        {
            var point = queue.Dequeue();

            if (point == end)
                break;

            foreach (var neighbor in point.ToDirectionalNeighbours())
            {
                if (!input.InRectangle(neighbor))
                    continue;

                if (input.Get(neighbor) == '#')
                    continue;

                var neighbourCost = costs[point] + 1;
                if (neighbourCost >= costs.GetValueOrDefault(neighbor, int.MaxValue))
                    continue;

                costs[neighbor] = neighbourCost;
                queue.Enqueue(neighbor);
            }
        }

        return costs;
    }

    private static Point GetPoint(char[][] input, char symbol)
    {
        foreach (var (x, y, item) in input.Iterate())
            if (item == symbol)
                return new Point(x, y);

        throw new ArgumentException("Symbol not found");
    }

    protected override char[][] ParseInputPart1(string input) =>
        input.Split('\n').Select(arr => arr.ToCharArray()).ToArray();
}