namespace AoC.Year2024.Day12;

public class Solution : Day<char[][]>
{
    private record Plot(HashSet<Point> Region, List<Point> PerimeterRegion)
    {
        public int Area => Region.Count;
        public int Perimeter => PerimeterRegion.Count;
    }

    protected override object DoPart1(char[][] input) => 
        CalculatePlots(input).Sum(plot => plot.Area * plot.Perimeter);

    protected override object DoPart2(char[][] input) => 
        CalculatePlots(input).Sum(plot => plot.Area * CountEdges(plot));
    
    private static List<Plot> CalculatePlots(char[][] input)
    {
        var plots = new List<Plot>();

        for (var y = 0; y < input.Length; y++)
        {
            for (var x = 0; x < input[y].Length; x++)
            {
                var point = new Point(x, y);

                if (plots.Exists(p => p.Region.Contains(point)))
                    continue;

                plots.Add(FloodFill(input, point));
            }
        }

        return plots;
    }

    private static Plot FloodFill(char[][] input, Point startPoint)
    {
        var plant = input.Get(startPoint);

        var plot = new HashSet<Point>();
        var perimeter = new List<Point>();
        var queue = new Queue<Point>([startPoint]);

        while (queue.Count > 0)
        {
            var point = queue.Dequeue();

            if (plot.Contains(point))
                continue;

            if (!input.InRectangle(point))
            {
                perimeter.Add(point);
                continue;
            }

            if (input.Get(point) != plant)
            {
                perimeter.Add(point);
                continue;
            }

            plot.Add(point);

            foreach (var neighbour in point.ToDirectionalNeighbours())
                queue.Enqueue(neighbour);
        }

        return new Plot(plot, perimeter);
    }
    
    private static int CountEdges(Plot plot)
    {
        var min = new Point(plot.Region.Select(p => p.X).Min(), plot.Region.Select(p => p.Y).Min());
        var max = new Point(plot.Region.Select(p => p.X).Max(), plot.Region.Select(p => p.Y).Max());

        var edges = 0;
        for (var y = min.Y; y <= max.Y; y++)
        {
            for (var x = min.X; x <= max.X; x++)
            {
                var point = new Point(x, y);
                if (!plot.Region.Contains(point))
                    continue;

                var direction = Direction.Left;
                for (var i = 0; i < 4; i++, direction = direction.RotateRight90())
                {
                    if (!plot.Region.Contains(point.Move(direction)) && !plot.Region.Contains(point.Move(direction.RotateRight90())))
                        edges++;
                    else if (!plot.Region.Contains(point.Move(direction)) && plot.Region.Contains(point.Move(direction.ToPoint() + direction.RotateRight90().ToPoint())))
                        edges++;
                }
            }
        }

        return edges;
    }

    protected override char[][] ParseInputPart1(string input)
    {
        return input
            .Split("\n")
            .Select(line => line.ToCharArray())
            .ToArray();
    }
}