﻿using System.Reflection;

await new AocBuilder()
    .SetYear(2024)
    .SetDays(Enumerable.Range(1, 25).ToArray())
    .SetLogLevel(Logger.LogLevel.Solve)
    .SkipExamples()
    .Execute(Assembly.GetAssembly(typeof(Program))!);
