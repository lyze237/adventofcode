namespace AoC.Year2024.Day21;

public class Solution : Day<string[]>
{
    private readonly Dictionary<(char previousSymbol, char nextSymbol, int level), long> cache = [];

    private static readonly char[][] Numbers =
        new[] { "789", "456", "123", " 0A" }.Select(s => s.ToCharArray()).ToArray();

    private static readonly char[][] Directions =
        new[] { " ^A", "<v>" }.Select(s => s.ToCharArray()).ToArray();

    protected override object DoPart1(string[] input) =>
        input.Sum(line => Solve(line, 2));

    protected override object DoPart2(string[] input) =>
        input.Sum(line => Solve(line, 25));

    private long Solve(string line, int robots)
    {
        var calculatedMoves = $"A{CalculateMoves(Numbers, line)}";
        
        var sum = 0L;
        for (var i = 1; i < calculatedMoves.Length; i++)
            sum += CountMoves(calculatedMoves[i - 1], calculatedMoves[i], robots);
        
        var code = long.Parse(line[..^1]);
        return code * sum;
    }
    
    private long CountMoves(char previousSymbol, char nextSymbol, int level)
    {
        var key = (previousSymbol, nextSymbol, level);
        if (cache.TryGetValue(key, out var result))
            return result;

        var moves = CalculateMove(Directions, previousSymbol, nextSymbol);

        if (level == 1)
            return cache[key] = moves.Length;

        moves = $"A{moves}";
        result = 0;

        for (var i = 1; i < moves.Length; i++)
            result += CountMoves(moves[i - 1], moves[i], level - 1);

        return cache[key] = result;
    }
    
    private static string CalculateMoves(char[][] keyMap, string input)
    {
        input = $"A{input}";
        var result = "";

        for (var i = 1; i < input.Length; i++)
            result += CalculateMove(keyMap, input[i - 1], input[i]);

        return result;
    }

    private static string CalculateMove(char[][] keyMap, char from, char to)
    {
        var result = "";

        var target = keyMap.IndexOf(to);
        var current = keyMap.IndexOf(from);

        var delta = target - current;

        while (true)
        {
            foreach (var symbol in "<^v>")
            {
                if (delta == new Point(0, 0))
                    return $"{result}A";

                var direction = symbol.DirectionFromArrow().ToPoint();

                var amount = direction.X == 0 ? delta.Y / direction.Y : delta.X / direction.X;
                if (amount <= 0)
                    continue;

                var dest = current + direction * amount;
                if (dest == keyMap.IndexOf(' '))
                    continue;

                current = dest;
                delta -= direction * amount;

                result += new string(symbol, (int)amount);
            }
        }
    }

    protected override string[] ParseInputPart1(string input) =>
        input.Split("\n");
}