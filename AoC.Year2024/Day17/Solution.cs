using AoC.Framework.Attributes;
using Microsoft.Z3;
using Status = Microsoft.Z3.Status;

namespace AoC.Year2024.Day17;

[SolutionSkipExamples]
public partial class Solution : Day<(Dictionary<string, int> registers, int[] instructions)>
{
    protected override object DoPart1((Dictionary<string, int> registers, int[] instructions) input)
    {
        var output = new List<int>();

        var pointer = 0;
        while (pointer < input.instructions.Length)
        {
            var instruction = input.instructions[pointer];
            var operand = input.instructions[pointer + 1];

            var allowPointerIncrement = true;

            switch (instruction)
            {
                case 0: // adv
                    var numerator = input.registers["A"];
                    var denominator = Math.Pow(2, ConvertToCombo(operand, input.registers));

                    input.registers["A"] = (int)(numerator / denominator);
                    break;
                case 1: // bxl
                    input.registers["B"] ^= operand;
                    break;
                case 2: // bst
                    input.registers["B"] = ConvertToCombo(operand, input.registers) % 8;
                    break;
                case 3: // jnz
                    if (input.registers["A"] == 0)
                        break;

                    allowPointerIncrement = false;
                    pointer = operand;
                    break;
                case 4: // bxc
                    input.registers["B"] ^= input.registers["C"];
                    break;
                case 5: // out
                    output.Add(ConvertToCombo(operand, input.registers) % 8);
                    break;
                case 6: // bdv
                    numerator = input.registers["A"];
                    denominator = Math.Pow(2, ConvertToCombo(operand, input.registers));

                    input.registers["B"] = (int)(numerator / denominator);
                    break;
                case 7: // cdv
                    numerator = input.registers["A"];
                    denominator = Math.Pow(2, ConvertToCombo(operand, input.registers));

                    input.registers["C"] = (int)(numerator / denominator);
                    break;
            }

            if (allowPointerIncrement)
                pointer += 2;
        }

        return string.Join(",", output);
    }

    protected override object DoPart2((Dictionary<string, int> registers, int[] instructions) input)
    {
        using var ctx = new Context();
        // Create the optimizer
        var opt = ctx.MkOptimize();

        var range = ctx.MkBVConst("range", 64);
        
        var a = range;

        foreach (var x in input.instructions)
        {
            // 2,4
            // bst [a]
            // b = a % 8
            var b = ctx.MkBVURem(a, ctx.MkBV(8, 64));

            // 1,1
            // bxl 1
            // b = b ^ 1
            b = ctx.MkBVXOR(b, ctx.MkBV(1, 64));

            // 7,5
            // cdv [b]
            // c = a / (1 << b)
            var c = ctx.MkBVUDiv(a, ctx.MkBVSHL(ctx.MkBV(1, 64), b));

            // 0,3
            // adv 3
            // a = a / Math.pow(2, 3)
            a = ctx.MkBVUDiv(a, ctx.MkBV(8, 64));

            // 1,4
            // bxl 4
            // b = b ^ 4
            b = ctx.MkBVXOR(b, ctx.MkBV(4, 64));

            // 4,0
            // bxl [c]
            // b = b ^ c
            b = ctx.MkBVXOR(b, c);

            // 5,5
            // out [b]
            // (b % 8) == x
            opt.Add(ctx.MkEq(ctx.MkBVURem(b, ctx.MkBV(8, 64)), ctx.MkBV(x, 64)));
        }

        // a == 0
        opt.Add(ctx.MkEq(a, ctx.MkBV(0, 64)));

        opt.MkMinimize(range);

        if (opt.Check() != Status.SATISFIABLE) 
            throw new ArgumentException("No solution found.");

        return opt.Model.Eval(range, true).ToString();
    }

    private static int ConvertToCombo(int operand, Dictionary<string, int> registers)
    {
        return operand switch
        {
            >= 0 and <= 3 => operand,
            4 => registers["A"],
            5 => registers["B"],
            6 => registers["C"],
            7 => throw new ArgumentException("Not a combo operand"),
            _ => throw new ArgumentException("Not a valid operand")
        };
    }

    protected override (Dictionary<string, int> registers, int[] instructions) ParseInputPart1(string input)
    {
        var registers = RegisterRegex().Matches(input)
            .ToDictionary(match => match.Groups["name"].Value, match => match.Groups["value"].Value.ToInt());

        var program = ProgramRegex().Match(input).Groups["values"].Value.Split(',').Select(v => v.ToInt()).ToArray();

        return (registers, program);
    }

    [GeneratedRegex(@"Register (?<name>\w+): (?<value>\d+)")]
    private static partial Regex RegisterRegex();

    [GeneratedRegex(@"Program: (?<values>[\d+,]+)")]
    private static partial Regex ProgramRegex();
}