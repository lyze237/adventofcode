namespace AoC.Year2024.Day18;

public class Solution : Day<Point[]>
{
    protected override object DoPart1(Point[] input)
    {
        var isExample = input.Length < 30;

        var size = isExample ? new Point(7, 7) : new Point(71, 71);
        var bytes = isExample ? 12 : 1024;

        var blockedOff = input.Take(bytes).ToArray();
        return BreadthFirstSearch(new Point(0, 0), size - 1, blockedOff, size);
    }

    protected override object DoPart2(Point[] input)
    {
        var isExample = input.Length < 30;
        var size = isExample ? new Point(7, 7) : new Point(71, 71);
        
        ProgressReporter?.SetSteps(input.Length);

        var index = ParallelEnumerable.Range(0, input.Length)
            .Select(i =>
            {
                ProgressReporter?.Step();
                return (index: i, result: BreadthFirstSearch(new Point(0, 0), size - 1, input.Take(i).ToArray(), size));
            })
            .ToArray()
            .Where(i => i.result == -1)
            .MinBy(i => i.index).index - 1;
        return $"{input[index].X},{input[index].Y}";
    }

    private static int BreadthFirstSearch(Point start, Point end, Point[] toAvoid, Point size)
    {
        var visited = new HashSet<Point>();
        
        var queue = new Queue<(Point Position, int Steps)>();
        queue.Enqueue((start, 0));
        visited.Add(start);

        while (queue.Count > 0)
        {
            var (point, steps) = queue.Dequeue();

            if (point == end)
                return steps;

            foreach (var neighbor in point.ToDirectionalNeighbours())
            {
                if (!neighbor.InRectangle(0, 0, size.X, size.Y) || toAvoid.Contains(neighbor) || visited.Contains(neighbor)) 
                    continue;
                
                queue.Enqueue((neighbor, steps + 1));
                visited.Add(neighbor);
            }
        }

        return -1;
    }

    protected override Point[] ParseInputPart1(string input) =>
        input.Split("\n")
            .Select(line => line.Split(","))
            .Select(splitted => new Point(splitted[0].ToInt(), splitted[1].ToInt()))
            .ToArray();
}