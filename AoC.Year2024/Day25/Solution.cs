using AoC.Framework.Attributes;

namespace AoC.Year2024.Day25;

[SolutionSkipExamples]
public class Solution : Day<(int[][] keys, int[][] locks)>
{
    protected override object DoPart1((int[][] keys, int[][] locks) input) => 
        input.locks.Sum(lok => CheckLock(lok, input.keys));

    private static long CheckLock(int[] lok, int[][] keys) => 
        keys
            .Select(key => !key.Where((t, i) => lok[i] + t > 5).Any())
            .Count(valid => valid);

    protected override object DoPart2((int[][] keys, int[][] locks) input) => 1;

    protected override (int[][] keys, int[][] locks) ParseInputPart1(string input)
    {
        var keys = new List<int[]>();
        var locks = new List<int[]>();

        foreach (var section in input.Split("\n\n"))
        {
            var lines = section.Split("\n");
            var length = lines[0].Length;

            var isKey = lines[^1].All(c => c == '#');

            if (isKey)
                lines = lines.Reverse().ToArray();

            var result = Enumerable.Range(0, length)
                .Select(x => Enumerable.Range(1, lines.Length - 1)
                    .Count(y => lines[y][x] == '#'))
                .ToArray();
            
            if (isKey)
                keys.Add(result);
            else
                locks.Add(result);
        }

        return (keys.ToArray(), locks.ToArray());
    }
}