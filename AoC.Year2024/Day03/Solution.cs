namespace AoC.Year2024.Day03;

public partial class Solution : Day
{
    [GeneratedRegex(@"mul\((?<left>\d+),(?<right>\d+)\)")]
    private static partial Regex Part1Regex();
    
    [GeneratedRegex(@"(?<mul>mul\((?<left>\d+),(?<right>\d+)\))|(?<do>do\(\))|(?<dont>don't\(\))")]
    private static partial Regex Part2Regex();
    
    public override object DoPart1(string input) => 
        Part1Regex()
            .Matches(input)
            .Select(m => m.Groups["left"].Value.ToInt() * m.Groups["right"].Value.ToInt())
            .Sum();

    public override object DoPart2(string input)
    {
        var enabled = true;
        var result = 0;
        
        foreach (Match match in Part2Regex().Matches(input))
        {
            if (match.Groups["do"].Success)
                enabled = true;
            else if (match.Groups["dont"].Success)
                enabled = false;
            
            if (enabled && match.Groups["mul"].Success)
                result += match.Groups["left"].Value.ToInt() * match.Groups["right"].Value.ToInt();
        }

        return result;
    }
}