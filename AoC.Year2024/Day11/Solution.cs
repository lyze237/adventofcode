using System.Collections.Concurrent;

namespace AoC.Year2024.Day11;

public class Solution : Day<List<ulong>>
{
    private readonly ConcurrentDictionary<(ulong stone, int steps), ulong> cache = [];

    protected override object DoPart1(List<ulong> input) =>
        Blink(input, 25);

    protected override object DoPart2(List<ulong> input) =>
        Blink(input, 75);

    private ulong Blink(List<ulong> input, int steps) => 
        input.Aggregate<ulong, ulong>(0, (current, stone) => current + BlinkStone(stone, steps));

    private ulong BlinkStone(ulong stone, int steps)
    {
        if (steps == 0)
            return 1;
        
        if (cache.TryGetValue((stone, steps), out var cachedValue))
            return cachedValue;

        ulong result;
        var stoneStr = stone.ToString();
        if (stone == 0)
        {
            result = BlinkStone(1, steps - 1);
        }
        else if (stoneStr.Length % 2 == 0)
        {
            var halfLength = stoneStr.Length / 2;
            var left = stoneStr[..halfLength].ToULong();
            var right = stoneStr[halfLength..].ToULong();
            
            return BlinkStone(left, steps - 1) + BlinkStone(right, steps - 1);
        }
        else
        {
            result = BlinkStone(stone * 2024, steps - 1);
        }

        return cache[(stone, steps)] = result;
    }
    
    protected override List<ulong> ParseInputPart1(string input) =>
        input
            .Split(" ")
            .Select(ulong.Parse)
            .ToList();
}