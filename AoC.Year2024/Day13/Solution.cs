namespace AoC.Year2024.Day13;

public partial class Solution : Day<Solution.ClawMachine[]>
{
    public record ClawMachine(Point A, Point B, Point Prize)
    {
        public ClawMachine(Match machine) : this(
            new Point(machine.Groups["aX"].Value.ToLong(), machine.Groups["aY"].Value.ToLong()),
            new Point(machine.Groups["bX"].Value.ToLong(), machine.Groups["bY"].Value.ToLong()),
            new Point(machine.Groups["prizeX"].Value.ToLong(), machine.Groups["prizeY"].Value.ToLong()))
        {
        }
    }

    [GeneratedRegex(@"Button A: X(?<aX>[+-]\d+), Y(?<aY>[+-]\d+)\nButton B: X(?<bX>[+-]\d+), Y(?<bY>[+-]\d+)\nPrize: X=(?<prizeX>\d+), Y=(?<prizeY>\d+)")]
    private static partial Regex ClawMachineRegex();

    protected override object DoPart1(ClawMachine[] input) => 
        input.Sum(m => WinClawMachine(m));

    protected override object DoPart2(ClawMachine[] input) =>
        input.Sum(m => WinClawMachine(m, 10000000000000));
    
    private static long WinClawMachine(ClawMachine machine, long shift = 0)
    {
        machine = machine with { Prize = machine.Prize + shift };
            
        var det = machine.A.Det(machine.B);
        var a = machine.Prize.Det(machine.B) / det;
        var b = machine.A.Det(machine.Prize) / det;

        if (machine.A * a + machine.B * b == machine.Prize)
            return 3 * a + 1 * b;
        
        return 0;
    }

    protected override ClawMachine[] ParseInputPart1(string input) =>
        ClawMachineRegex()
            .Matches(input)
            .Select(machine => new ClawMachine(machine))
            .ToArray();
}