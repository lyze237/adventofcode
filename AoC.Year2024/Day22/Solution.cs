namespace AoC.Year2024.Day22;

public class Solution : Day<int[]>
{
    protected override object DoPart1(int[] input) =>
        input.Sum(startSecret => GenerateSecretSequence(startSecret, 2000).secrets[^1]);

    protected override object DoPart2(int[] input)
    {
        var sequences = new Dictionary<(long, long, long, long), long>();
        
        ProgressReporter?.SetSteps(input.Length);

        foreach (var (_, changes) in input.Select(startSecret => GenerateSecretSequence(startSecret, 2000)))
        {
            var seen = new HashSet<(long, long, long, long)>();

            for (var i = 3; i < changes.Count; i++)
            {
                var sequence = ToQuadruple(changes
                    .Select(c => c.difference)
                    .Skip(i - 3)
                    .Take(4)
                    .ToArray());
                
                if (seen.Contains(sequence))
                    continue;

                sequences[sequence] = sequences.GetValueOrDefault(sequence, 0) + changes[i].price;
                seen.Add(sequence);
            }
            
            ProgressReporter?.Step();
        }

        return sequences.Values.Max();
    }
    
    private static (List<long> secrets, List<(long price, long difference)> changes) GenerateSecretSequence(long startSecret, int length)
    {
        var secrets = new List<long> { startSecret };
        var differences = new List<(long price, long difference)>();

        var secret = startSecret;
        for (var i = 0; i < length; i++)
        {
            var original = secret;
            secret = MutateSecret(secret);
            
            var originalPrice = original % 10;
            var price = secret % 10;

            secrets.Add(secret);
            differences.Add((price, price - originalPrice));
        }

        return (secrets, differences);
    }

    private static (long, long, long, long) ToQuadruple(long[] numbers) => 
        (numbers[0], numbers[1], numbers[2], numbers[3]);
    
    private static long MutateSecret(long secret)
    {
        secret = MixSecret(secret, secret * 64);
        secret = PruneNumber(secret);

        secret = MixSecret(secret, secret / 32);
        secret = PruneNumber(secret);

        secret = MixSecret(secret, secret * 2048);
        secret = PruneNumber(secret);

        return secret;
    }

    private static long MixSecret(long secret, long number) =>
        number ^ secret;

    private static long PruneNumber(long secret) =>
        secret % 16777216;
    
    protected override int[] ParseInputPart1(string input) =>
        input
            .Split("\n")
            .Select(int.Parse)
            .ToArray();
}