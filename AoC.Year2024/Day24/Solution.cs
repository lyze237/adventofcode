namespace AoC.Year2024.Day24;

public partial class Solution : Day<(Dictionary<string, int> states, Solution.Gate[] gates)>
{
    public record Gate(string Left, string Right, string Operand, string Result)
    {
        public bool IsAnyInput(string input) => Left == input || Right == input;
        public bool IsResultOf(Gate gate) => Left == gate.Result || Right == gate.Result;
    }

    protected override object DoPart1((Dictionary<string, int> states, Gate[] gates) input)
    {
        var shouldLoop = true;
        while (shouldLoop)
        {
            shouldLoop = false;
            foreach (var (left, right, gate, result) in input.gates)
            {
                if (!input.states.ContainsKey(left) || !input.states.ContainsKey(right))
                {
                    shouldLoop = true;
                    continue;
                }

                input.states[result] = gate switch
                {
                    "OR" => input.states[left] | input.states[right],
                    "XOR" => input.states[left] ^ input.states[right],
                    "AND" => input.states[left] & input.states[right],
                    _ => throw new ArgumentOutOfRangeException($"Unknown gate: {gate}")
                };
            }
        }

        var number = input.states
            .Where(state => state.Key.StartsWith('z'))
            .OrderByDescending(state => state.Key)
            .Select(state => state.Value);

        return Convert.ToInt64(string.Join("", number), 2);
    }

    protected override object DoPart2((Dictionary<string, int> states, Gate[] gates) input)
    {
        var z = input.gates.Count(g => g.Result[0] == 'z');

        var notXorZ = input.gates
            .Where(g => g.Operand != "XOR")
            .Where(g => g.Result[0] == 'z')
            .Where(g => g.Result != $"z{z - 1}")
            .Select(g => g.Result)
            .ToList();

        var noXorInMiddle = input.gates
            .Where(g => g.Operand == "XOR")
            .Where(g => g.Result[0] != 'z')
            .Where(g => g.Left[0] is not ('x' or 'y'))
            .Where(g => g.Right[0] is not ('x' or 'y'))
            .Select(g => g.Result)
            .ToList();

        notXorZ.AddRange(noXorInMiddle);
        var invalid = notXorZ.ToHashSet();

        foreach (var left in input.gates.Where(g => g.Operand == "AND" && !g.IsAnyInput("x00")))
            foreach (var right in input.gates)
                if (right.IsResultOf(left) && right.Operand != "OR")
                    invalid.Add(left.Result);

        foreach (var left in input.gates.Where(g => g.Operand == "XOR"))
            foreach (var right in input.gates)
                if (right.IsResultOf(left) && right.Operand == "OR")
                    invalid.Add(left.Result);

        return string.Join(',', invalid.Order());
    }

    protected override (Dictionary<string, int> states, Gate[] gates) ParseInputPart1(string input)
    {
        var states = DataRegex()
            .Matches(input)
            .Select(line => (gate: line.Groups["gate"].Value, bit: line.Groups["bit"].Value.ToInt()))
            .ToDictionary(line => line.gate, line => line.bit);

        var gates = GateRegex()
            .Matches(input)
            .Select(line => new Gate(line.Groups["left"].Value, line.Groups["right"].Value, line.Groups["gate"].Value,
                line.Groups["result"].Value))
            .ToArray();

        return (states, gates);
    }

    [GeneratedRegex(@"(?<gate>\w\d+): (?<bit>\d)")]
    private static partial Regex DataRegex();

    [GeneratedRegex(@"(?<left>[\w\d]+) (?<gate>[\w]+) (?<right>[\w\d]+) -> (?<result>[\w\d]+)")]
    private static partial Regex GateRegex();
}