namespace AoC.Year2024.Day10;

public class Solution : Day<int[][]>
{
    protected override object DoPart1(int[][] input) => 
        FindStartPoints(input)
            .Select(startPoint => BreadthFirstSearch(startPoint, input).Distinct().Count())
            .Sum();

    protected override object DoPart2(int[][] input) => 
        FindStartPoints(input)
            .Select(startPoint => BreadthFirstSearch(startPoint, input).Count)
            .Sum();

    private static List<Point> GetNeighbours(Point point, int[][] input) => 
        point.ToDirectionalNeighbours()
            .Where(p => p.InRectangle(input))
            .Where(p => input.Get(p) - input.Get(point) == 1)
            .ToList();

    private static List<Point> BreadthFirstSearch(Point start, int[][] input)
    {
        var queue = new Queue<Point>();
        var ninerTrails = new List<Point>();

        queue.Enqueue(start);
        while (queue.Count > 0)
        {
            var point = queue.Dequeue();

            if (input.Get(point) == 9)
            {
                ninerTrails.Add(point);
                continue;
            }

            var neighbours = GetNeighbours(point, input);
            foreach (var neighbour in neighbours)
                queue.Enqueue(neighbour);
        }

        return ninerTrails;
    }

    private static List<Point> FindStartPoints(int[][] input)
    {
        var startPoints = new List<Point>();

        for (var y = 0; y < input.Length; y++)
            for (var x = 0; x < input[y].Length; x++)
                if (input[y][x] == 0)
                    startPoints.Add(new Point(x, y));

        return startPoints;
    }

    protected override int[][] ParseInputPart1(string input) =>
        input.Split("\n")
            .Select(line => line.ToCharArray().Select(c => c.ToInt()).ToArray())
            .ToArray();
}