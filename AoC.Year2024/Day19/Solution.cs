namespace AoC.Year2024.Day19;

public class Solution : Day<(string[] patterns, string[] designs)>
{
    protected override object DoPart1((string[] patterns, string[] designs) input)
    {
        var cache = new Dictionary<string, long>();

        return input.designs.Count(design => Search(design, input.patterns, cache) > 0);
    }

    protected override object DoPart2((string[] patterns, string[] designs) input)
    {
        var cache = new Dictionary<string, long>();

        return input.designs.Sum(design => Search(design, input.patterns, cache));
    }

    private static long Search(string design, string[] patterns, Dictionary<string, long> cache)
    {
        if (design.Length == 0)
            return 1;
        
        if (cache.TryGetValue(design, out var value))
            return value;

        return cache[design] = patterns
            .Where(design.StartsWith)
            .Sum(pattern => Search(design[pattern.Length..], patterns, cache));
    }

    protected override (string[] patterns, string[] designs) ParseInputPart1(string input)
    {
        var (patterns, designs) = input.Split("\n\n");
        return (patterns!.Split(", "), designs!.Split("\n"));
    }
}