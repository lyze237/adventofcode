namespace AoC.Year2024.Day09;

public class Solution : Day<int?[]>
{
    protected override object DoPart1(int?[] input)
    {
        while (true)
        {
            var filledSlotIndex = FindFilledSlot(input);
            if (filledSlotIndex == null)
                throw new Exception("No filled slot found???");

            var emptySlotIndex = FindEmptySlotsForFileLength(input, 1);
            if (emptySlotIndex == null)
                throw new Exception("No empty slot found???");

            input[emptySlotIndex.Value] = input[filledSlotIndex.Value];
            input[filledSlotIndex.Value] = null;

            if (IsValidMap(input))
                return CalculateChecksum(input);
        }
    }
    
    protected override object DoPart2(int?[] input) 
    {
        var highestId = input.Where(i => i != null).OrderByDescending(i => i).Cast<int>().First();
        
        ProgressReporter?.SetSteps(highestId);

        for (var id = highestId; id >= 0; id--)
        {
            var idIndex = input.IndexOf(id);
            var idFileSize = input.Count(i => i == id);
            
            var emptySlotIndex = FindEmptySlotsForFileLength(input, idFileSize);
            if (emptySlotIndex == null)
            {
                ProgressReporter?.Step();
                continue;
            }

            if (idIndex < emptySlotIndex)
            {
                ProgressReporter?.Step();
                continue;
            }

            for (var i = 0; i < idFileSize; i++)
            {
                input[emptySlotIndex.Value + i] = id;
                input[idIndex + i] = null;
            }
            
            ProgressReporter?.Step();
        }

        return CalculateChecksum(input);
    }

    private static bool IsValidMap(int?[] input)
    {
        var list = input.ToList();

        var empty = list.IndexOf(null);
        return list.Skip(empty).All(i => i == null);
    }

    private static int? FindFilledSlot(int?[] input)
    {
        for (var i = input.Length - 1; i >= 0; i--)
            if (input[i] != null)
                return i;

        return null;
    }

    private static int? FindEmptySlotsForFileLength(int?[] input, int length)
    {
        for (var i = 0; i <= input.Length - length; i++)
        {
            var isGap = true;
            for (var j = 0; j < length; j++)
            {
                if (input[i + j] == null) 
                    continue;
                
                isGap = false;
                break;
            }

            if (isGap)
                return i;
        }

        return null;
    }

    private static ulong CalculateChecksum(int?[] input)
    {
        ulong checksum = 0;

        for (uint i = 0; i < input.Length; i++)
        {
            var id = input[i];
            if (id == null)
                continue;

            checksum += (uint) id.Value * i;
        }

        return checksum;
    }

    protected override int?[] ParseInputPart1(string input)
    {
        var blocks = input.ToCharArray();

        var map = new List<int?>();

        var id = 0;
        for (var i = 0; i < blocks.Length; i++)
        {
            var block = Convert.ToInt32(blocks[i].ToString());
            if (i % 2 == 0)
            {
                for (var j = 0; j < block; j++)
                    map.Add(id);
                id++;
            }
            else
            {
                for (var j = 0; j < block; j++)
                    map.Add(null);
            }
        }

        return map.ToArray();
    }
}