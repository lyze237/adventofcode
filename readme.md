# Advent of Code

These are all my solutions I've done for Advent of Code.

* 2024 => 50 ⭐
* 2023 => 37 ⭐
* 2022 => 31 ⭐
* 2021 =>  4 ⭐
* 2018 => 28 ⭐
* 2017 => 50 ⭐ 

Some of the older projects have a couple broken days. I don't know why that happened is the case but I'll fix them in the future.

# AoC.Framework

This is my little helper library for my [Advent of Code](https://adventofcode.com) adventure.

It:
1. Fetches the input
2. Tries to extract example input and result
3. Saves those results so I can edit them
4. Runs one or multiple days in either an interactive or progress bar based mode
5. Submits results if needed

# Disclaimer

This library does follow the automation guidelines on the /r/adventofcode [community wiki](https://www.reddit.com/r/adventofcode/wiki/faqs/automation). 

Specifically:

* Outbound calls are cached and will only happen when something changes (For /20XX/day/XX). See `AoC.Framework/AoCHttpClient.cs` and `AoC.Framework/AoCCache.cs`
  * More specifically, the script downloads the page:
    * once at the beginning
    * once I submit part 1 correctly
    * once I submit part 2 correctly
  * Then it knows all the answers and won't bother the website anymore
* Failed tries are also cached and won't bother the api anymore unless the "retry" or "wrong page?" error happens. See `AoC.Framework/AocPageParser.cs`
* Once inputs are downloaded, they are cached locally. See `AoC.Framework/AoCHttpClient.cs` and `AoC.Framework/AoCCache.cs`
* The User-Agent header in `AoC.Framework/AoCBrowser.cs` is set to this repo
