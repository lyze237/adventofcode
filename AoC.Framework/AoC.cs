﻿using System.Reflection;
using AoC.Framework.Attributes;
using AoC.Framework.Logging;
using AoC.Framework.Renderers;
using Spectre.Console;

namespace AoC.Framework;

public class AoC(Assembly assembly)
{
    private Assembly Assembly { get; } = assembly;

    public async Task ExecuteInteractive(int year, int[] days, bool skipExamples = false)
    {
        foreach (var day in days)
        {
            var solution = FindAndCreateSolutionInstance(day);
            solution.SetInteractive();

            for (var part = 1; part <= 2; part++)
                await WrapDoPartInteractive(new ProgressTask(part, GetDayName(solution, year, day, part), 2), solution, year, day, part, skipExamples);
        }
    }

    private static async Task WrapDoPartInteractive(ProgressTask task, Day solution, int year, int day, int part,
        bool skipExamples)
    {
        await WrapDoPart(task, solution, year, day, part, skipExamples);

        AnsiConsole.Write(new BarChart()
            .Width(60)
            .Label(task.Description)
            .WithMaxValue(100)
            .UseValueFormatter(d => $"{d}%")
            .AddItem("Progress", task.Percentage, Color.Green));

        AnsiConsole.MarkupLine("[yellow]Press any key to continue[/]");
        await AnsiConsole.Console.Input.ReadKeyAsync(false, CancellationToken.None);
    }

    public async Task Execute(int year, int[] days, bool skipExamples = false)
    {
        await AnsiConsole.Progress()
            .Columns(new TaskDescriptionColumn
                {
                    Alignment = Justify.Left
                },
                new ProgressBarColumn(),
                new PercentageColumn(),
                new ElapsedTimeWithMillisecondsColumn())
            .StartAsync(async ctx =>
            {
                foreach (var day in days)
                {
                    var solution = FindAndCreateSolutionInstance(day);

                    for (var part = 1; part <= 2; part++)
                        await WrapDoPart(ctx.AddTask(GetDayName(solution, year, day, part)), solution, year, day, part, skipExamples);
                }
            });
    }

    private static async Task WrapDoPart(ProgressTask task, Day solution, int year, int day, int part, bool skipExamples)
    {
        try
        {
            await DoPart(solution, year, day, part, task, skipExamples);
        }
        catch (Exception e)
        {
            task.Description += ": [red][b]FAILED[/][/]";
            task.StopTask();
            AnsiConsole.WriteException(e);
        }
    }

    private Day FindAndCreateSolutionInstance(int day)
    {
        var dayType = Assembly
            .GetTypes()
            .ToList()
            .Find(t => (t.Namespace ?? "").EndsWith($"Day{day:00}"));

        if (dayType == null)
            throw new ArgumentException($"Day {day:00} not found in {Assembly.FullName}");

        var solutionInstance = Activator.CreateInstance(dayType);

        if (solutionInstance is not Day solution)
            throw new ArgumentException($"Day {day:00} is not a Day");

        return solution;
    }

    private static async Task DoPart(Day solution, int year, int day, int part, ProgressTask task, bool skipExamples)
    {
        task.Description = GetDayName(solution, year, day, part);

        if (skipExamples || ShouldSkipTests(solution, part))
        {
            task.MaxValue = 1;
        }
        else
        {
            task.MaxValue = 2;
            await DoExamplePart(solution, year, day, part, task);
        }

        await DoActualPart(solution, year, day, part, task);
    }

    private static async Task DoExamplePart(Day solution, int year, int day, int part, ProgressTask task)
    {
        Logger.LogKey($"Running {PartToString(year, day, part)} Example");
        var exampleInput = await AocPageParser.GetOrDownloadInputAsync(year, day, part, true);

        var expectedExampleAnswer = await AocPageParser.GetOrDownloadExampleAnswer(year, day, part);

        var exampleReporter = CreateProgressReporter(task);
        solution.SetProgressReporter(exampleReporter);
        var exampleAnswer = part switch
        {
            1 => solution.DoExamplePart1(exampleInput),
            2 => solution.DoExamplePart2(exampleInput),
            _ => throw new ArgumentException($"Invalid part {part}")
        };

        if (!exampleReporter.Stepped)
            task.Increment(1);

        if ((exampleAnswer.ToString() ?? "").Trim() != expectedExampleAnswer.Trim())
        {
            Logger.LogError($"Example answer {exampleAnswer} does not match {expectedExampleAnswer}");
            throw new ArgumentException($"Example answer {PartToString(year, day, part)} does not match expected answer: {exampleAnswer} != {expectedExampleAnswer}");
        }

        Logger.LogKey($"Example answer {exampleAnswer} matches {expectedExampleAnswer}");
    }
    
    private static async Task DoActualPart(Day solution, int year, int day, int part, ProgressTask task)
    {
        var input = await AocPageParser.GetOrDownloadInputAsync(year, day, part, false);
        Logger.LogKey($"Running {PartToString(year, day, part)}");

        var reporter = CreateProgressReporter(task);
        solution.SetProgressReporter(reporter);
        var answer = part switch
        {
            1 => solution.DoPart1(input),
            2 => solution.DoPart2(input),
            _ => throw new ArgumentException($"Invalid part {part}")
        };

        if (!reporter.Stepped)
            task.Increment(1);

        var response = await AocPageParser.GetAnswerOnPage(year, day, part);
        if (response == null)
        {
            Logger.LogDebug($"Submitting {answer} as answer");
            response = await AocPageParser.SubmitAnswerAsync(year, day, part, answer.ToString() ?? throw new ArgumentException("Answer null exception"));
            if (response.Contains("That's not the right answer"))
            {
                Logger.LogError($"Answer {answer} was not the right solution: {response}");
                throw new ArgumentException($"Answer {PartToString(year, day, part)} did not succeed: {answer}");
            }
        }
        else
        {
            if (answer.ToString() != response.Trim())
            {
                Logger.LogError($"Answer {answer} did not match {response}");
                throw new ArgumentException($"Answer {PartToString(year, day, part)} does not match expected answer: {answer} != {response}");
            }
        }

        task.Value = int.MaxValue;
        task.Description += $": [green][b]{answer}[/][/]";
        task.StopTask();
        Logger.LogKey($"Answer {answer} is correct!");
    }

    private static string PartToString(int year, int day, int part) =>
        $"Day {year}-{day:00} Part {part}";

    private static TaskProgressReporter CreateProgressReporter(ProgressTask task) =>
        new(progress => { task.Increment(progress.Normalize()); });

    private static bool ShouldSkipTests(Day day, int part)
    {
        var solutionSkipExamplesAttribute = day.GetType().GetCustomAttribute<SolutionSkipExamplesAttribute>();
        if (solutionSkipExamplesAttribute == null)
            return false;

        return part switch
        {
            1 => solutionSkipExamplesAttribute.SkipPart1,
            2 => solutionSkipExamplesAttribute.SkipPart2,
            _ => throw new ArgumentException($"Invalid part {part}")
        };
    }

    private static string GetDayName(Day solution, int year, int day, int part)
    {
        var solutionName = solution.GetType().GetCustomAttribute<SolutionNameAttribute>()?.Name;
        if (solutionName != null)
            return $"{PartToString(year, day, part)}: {solutionName}";
        
        return $"Day {day:00} Part {part}";
    }
}