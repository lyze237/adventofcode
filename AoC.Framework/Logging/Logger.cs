using Spectre.Console;

namespace AoC.Framework.Logging;

public class Logger
{
    public static LogLevel MinLogLevel { get; set; } = LogLevel.Debug;

    public static void LogDebug(string message) =>
        Log(LogLevel.Debug, message);

    public static void LogSolution(string message) =>
        Log(LogLevel.Solve, message);
    
    public static void LogInfo(string message) =>
        Log(LogLevel.Info, message);

    public static void LogKey(string message) =>
        Log(LogLevel.Key, message);

    public static void LogError(string message) =>
        Log(LogLevel.Error, message);

    public static void Log(LogLevel level, string message)
    {
        if (level >= MinLogLevel)
            AnsiConsole.MarkupLine($"[{level.GetColor()}][[{level.ToString(),-5}]][/] {message.EscapeMarkup()}");
    }

    public enum LogLevel
    {
        Debug = 0,
        Solve = 1,
        Info = 2,
        Key = 3,
        Error = 4
    }
}

public static class LogLevelExtensions
{
    public static string GetColor(this Logger.LogLevel level)
    {
        return level switch
        {
            Logger.LogLevel.Debug => ConsoleColor.Gray.ToString(),
            Logger.LogLevel.Solve => ConsoleColor.White.ToString(),
            Logger.LogLevel.Info => ConsoleColor.White.ToString(),
            Logger.LogLevel.Key => ConsoleColor.Cyan.ToString(),
            Logger.LogLevel.Error => ConsoleColor.Red.ToString(),
            _ => throw new ArgumentOutOfRangeException(nameof(level), level, null)
        };
    }
}