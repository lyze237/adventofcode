namespace AoC.Framework;

public class TaskProgressReporter(Action<TaskProgressReporter> onProgress)
{
    public double Steps { get; set; }

    public bool Stepped { get; private set; }
    
    public void Step()
    {
        Stepped = true;
        onProgress(this);
    }

    public double Normalize() => 
        1d / Steps;

    public void SetSteps(double steps) => 
        Steps = steps;
}