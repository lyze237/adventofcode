using System.Text.Json;

namespace AoC.Framework;

public static class AocCache
{
    public record Output(string Answer, string Result);
    
    public static string? ReadPageFile(int day)
    {
        var file = GetPageFile(day);
        return File.Exists(file) ? File.ReadAllText(file) : null;
    }
    
    public static string? ReadInputFile(int day, int part, bool example)
    {
        var file = GetInputFile(day, part, example);
        return File.Exists(file) ? File.ReadAllText(file) : null;
    }

    public static string? ReadExampleAnswerFile(int day, int part)
    {
        var file = GetExampleAnswerFile(day, part);
        return File.Exists(file) ? File.ReadAllText(file) : null;
    }
    
    public static string? ReadAnswerFile(int day, int part, string answer)
    {
        var file = GetAnswerFile(day, part);
        if (!File.Exists(file))
            return null;
        
        var text = File.ReadAllText(file);
        var json = JsonSerializer.Deserialize<Output[]>(text);
        return json?.FirstOrDefault(o => o.Answer == answer)?.Result;
    }
    
    public static void WritePageFile(int day, string content)
    {
        var file = GetPageFile(day);
        File.WriteAllText(file, content);
    }
    
    public static void WriteInputFile(int day, int part, bool example, string content)
    {
        var file = GetInputFile(day, part, example);
        File.WriteAllText(file, content);
    }
    
    public static void WriteExampleAnswerFile(int day, int part, string content)
    {
        var file = GetExampleAnswerFile(day, part);
        File.WriteAllText(file, content);
    }
    
    public static void WriteAnswerFile(int day, int part, string answer, string result)
    {
        var file = GetAnswerFile(day, part);

        var output = new List<Output>();
        if (File.Exists(file))
        {
            var text = File.ReadAllText(file);
            output = JsonSerializer.Deserialize<Output[]>(text)!.ToList();
        }
        
        output.Add(new Output(answer, result));
        File.WriteAllText(file, JsonSerializer.Serialize(output));
    }

    
    private static string GetInputFile(int day, int part, bool example) =>
        EnsureDirectoryExists(Path.Combine(GetWorkingDirectory(day), example ? $"part{part}.example.input.txt" : $"part{part}.input.txt"));
    
    private static string GetExampleAnswerFile(int day, int part) =>
        EnsureDirectoryExists(Path.Combine(GetWorkingDirectory(day), $"part{part}.example.result.txt"));
    private static string GetAnswerFile(int day, int part) =>
        EnsureDirectoryExists(Path.Combine(GetWorkingDirectory(day), $"part{part}.results.json"));
    
    private static string GetPageFile(int day) => 
        EnsureDirectoryExists(Path.Combine(GetWorkingDirectory(day), "page.html"));

    private static string GetWorkingDirectory(int day) =>
        Path.Combine($"Day{day:00}", "files");

    private static string EnsureDirectoryExists(string path)
    {
        var directory = Path.GetDirectoryName(path);
        if (directory == null)
            throw new ArgumentException($"Directory of path {path} is null");
        
        if (!Directory.Exists(directory))
            Directory.CreateDirectory(directory);

        return path;
    }
}