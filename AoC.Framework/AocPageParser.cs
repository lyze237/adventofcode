using System.Web;
using AoC.Framework.Logging;
using HtmlAgilityPack;

namespace AoC.Framework;

public static class AocPageParser
{
    public static async Task<string> GetOrDownloadPageContentAsync(int year, int day, bool forceDownload = false)
    {
        var httpClient = new AoCBrowser();
        if (!forceDownload)
        {
            var cache = AocCache.ReadPageFile(day);
            if (cache != null)
                return cache;
        }

        var page = await httpClient.DownloadPage(year, day);
        AocCache.WritePageFile(day, page);
        return page;
    }

    public static async Task<string?> GetAnswerOnPage(int year, int day, int part)
    {
        var page = await GetOrDownloadPageContentAsync(year, day);
        var doc = new HtmlDocument();
        doc.LoadHtml(page);

        var answerNode = doc.DocumentNode.SelectSingleNode($"//article[@class='day-desc'][{part}]/following-sibling::p/code[not(@class='puzzle-input')]");
        return HttpUtility.HtmlDecode(answerNode?.InnerText);
    }

    public static async Task<string> SubmitAnswerAsync(int year, int day, int part, string answer)
    {
        var httpClient = new AoCBrowser();

        var resultPage = AocCache.ReadAnswerFile(day, part, answer) ?? await httpClient.SubmitAnswer(year, day, part, answer);

        var doc = new HtmlDocument();
        doc.LoadHtml(resultPage);

        var answerText = doc.DocumentNode.SelectSingleNode("//article").InnerText;
        if (answerText.Contains("You gave an answer too recently") || answerText.Contains("You don't seem to be solving the right level"))
            return answerText;

        AocCache.WriteAnswerFile(day, part, answer, resultPage);

        if (answerText.Contains("That's the right answer"))
            await GetOrDownloadPageContentAsync(year, day, true);

        return answerText;
    }

    public static async Task<string> GetOrDownloadInputAsync(int year, int day, int part, bool example)
    {
        var httpClient = new AoCBrowser();
        Logger.LogDebug($"Getting or downloading input for {year}-{day}-{part}{(example ? "-example" : "")}");

        var input = AocCache.ReadInputFile(day, part, example);
        if (input == null)
        {
            Logger.LogDebug($"Input file for {year}-{day}-{part} not found in cache, downloading");
            if (example)
            {
                Logger.LogDebug("Input is actually an example, parsing page for input");
                input = await ParsePageForExampleInput(year, day);
                
                // Force file creation
                try
                {
                    await GetOrDownloadExampleAnswer(year, day, part);
                }
                catch
                {
                    // ignored
                }
            }
            else
            {
                input = await httpClient.DownloadInput(year, day);
            }

            AocCache.WriteInputFile(day, part, example, input ?? "<Please fill this in>");
        }

        return input?.TrimEnd() ??
               throw new ArgumentNullException(
                   $"No input found for year {year} day {day} {(example ? "example" : "")}");
    }

    public static async Task<string> GetOrDownloadExampleAnswer(int year, int day, int part)
    {
        var input = AocCache.ReadExampleAnswerFile(day, part);
        if (input == null)
        {
            input = await ParsePageForExampleAnswer(year, day, part);

            AocCache.WriteExampleAnswerFile(day, part, input ?? "<Please fill this in>");
        }

        return input?.TrimEnd() ??
               throw new ArgumentNullException($"No example answer found for year {year} day {day} part {part}");
    }

    private static async Task<string?> ParsePageForExampleInput(int year, int day)
    {
        var page = await GetOrDownloadPageContentAsync(year, day);
        var doc = new HtmlDocument();
        doc.LoadHtml(page);


        var exampleNode = doc.DocumentNode.SelectSingleNode(
            "//article[@class='day-desc']/p[(contains(., 'For') or contains(., 'Here') or contains(., 'here') or contains(., 'for')) and (contains(., 'example') or contains(., 'Example')) and contains(., ':')]/following-sibling::pre/code");
        return HttpUtility.HtmlDecode(exampleNode?.InnerText.TrimEnd());
    }

    private static async Task<string?> ParsePageForExampleAnswer(int year, int day, int part)
    {
        var page = await GetOrDownloadPageContentAsync(year, day);
        var doc = new HtmlDocument();
        doc.LoadHtml(page);

        var solutionNode =
            doc.DocumentNode.SelectSingleNode($"//article[@class='day-desc'][{part}]/following-sibling::p/code");
        return solutionNode?.InnerText;
    }
}