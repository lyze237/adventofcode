using System.Net.Http.Headers;

namespace AoC.Framework;

public class AoCBrowser
{
    private readonly HttpClient httpClient = new();
    
    public AoCBrowser()
    {
        httpClient.BaseAddress = new Uri("https://adventofcode.com");
        httpClient.DefaultRequestHeaders.Add("Cookie", $"session={GetSession()}");
        httpClient.DefaultRequestHeaders.UserAgent.Add(new ProductInfoHeaderValue("(+https://gitlab.com/lyze237/AdventOfCode)"));
    }

    public async Task<string> DownloadInput(int year, int day)
    {
        var response = await httpClient.GetAsync($"/{year}/day/{day}/input");
        return await response.EnsureSuccessStatusCode().Content.ReadAsStringAsync();
    }

    public async Task<string> DownloadPage(int year, int day)
    {
        var response = await httpClient.GetAsync($"/{year}/day/{day}");
        return await response.EnsureSuccessStatusCode().Content.ReadAsStringAsync();
    }
    
    public async Task<string> SubmitAnswer(int year, int day, int part, string answer)
    {
        var response = await httpClient.PostAsync($"/{year}/day/{day}/answer", new FormUrlEncodedContent(new Dictionary<string, string>
        {
            ["level"] = part.ToString(),
            ["answer"] = answer,
        }));
        
        return await response.EnsureSuccessStatusCode().Content.ReadAsStringAsync();
    }
    
    private static string GetSession() => 
        Environment.GetEnvironmentVariable("AOC_SESSION") ?? throw new ArgumentException("AOC_SESSION not set.");
}
