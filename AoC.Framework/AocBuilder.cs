using System.Reflection;
using AoC.Framework.Logging;

namespace AoC.Framework;

public class AocBuilder
{
    private int year;
    private int[] days;
    private Logger.LogLevel logLevel = Logger.LogLevel.Debug;
    private bool interactive;
    private bool skipExamples;

    public AocBuilder SetYear(int year)
    {
        this.year = year;
        return this;
    }

    public AocBuilder SetDays(params int[] days)
    {
        this.days = days;
        return this;
    }

    public AocBuilder SetLogLevel(Logger.LogLevel logLevel)
    {
        this.logLevel = logLevel;
        return this;
    }

    public AocBuilder SetInteractive(bool interactive = true)
    {
        this.interactive = interactive;
        return this;
    }

    public AocBuilder SkipExamples(bool skipExamples = true)
    {
        this.skipExamples = skipExamples;
        return this;
    }

    public async Task Execute(Assembly assembly)
    {
        Logger.MinLogLevel = logLevel;
        var aoC = new AoC(assembly);
        if (interactive)
            await aoC.ExecuteInteractive(year, days, skipExamples);
        else
            await aoC.Execute(year, days, skipExamples);
    }
}