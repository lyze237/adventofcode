namespace AoC.Framework;

public abstract class Day
{
    protected TaskProgressReporter? ProgressReporter;
    protected bool Interactive;

    public abstract object DoPart1(string input);

    public virtual object DoExamplePart1(string input) =>
        DoPart1(input);

    public abstract object DoPart2(string input);
    public virtual object DoExamplePart2(string input) =>
        DoPart2(input);

    public void SetProgressReporter(TaskProgressReporter progressReporter) =>
        ProgressReporter = progressReporter;

    public void SetInteractive(bool interactive = true) =>
        Interactive = interactive;

    protected void ExecuteInteractive(Action action)
    {
        if (Interactive)
            action();
    }
}

public abstract class Day<TInput> : Day
{
    public override object DoPart1(string input) =>
        DoPart1(ParseInputPart1(input));

    public override object DoExamplePart1(string input) => 
        DoExamplePart1(ParseExampleInputPart1(input));

    public override object DoPart2(string input) => 
        DoPart2(ParseInputPart2(input));

    public override object DoExamplePart2(string input) => 
        DoExamplePart2(ParseExampleInputPart2(input));

    protected abstract object DoPart1(TInput input);
    protected abstract object DoPart2(TInput input);

    protected virtual object DoExamplePart1(TInput input)
        => DoPart1(input);
    
    protected virtual object DoExamplePart2(TInput input)
        => DoPart2(input);

    protected abstract TInput ParseInputPart1(string input);
    protected virtual TInput ParseExampleInputPart1(string input) => 
        ParseInputPart1(input);

    protected virtual TInput ParseInputPart2(string input) =>
        ParseInputPart1(input);
    protected virtual TInput ParseExampleInputPart2(string input) => 
        ParseInputPart2(input);
}