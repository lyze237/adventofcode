namespace AoC.Framework.Attributes;

[AttributeUsage(AttributeTargets.Class)]
public class SolutionSkipExamplesAttribute(bool skipPart1 = true, bool skipPart2 = true) : Attribute
{
    public bool SkipPart1 { get; } = skipPart1;
    public bool SkipPart2 { get; } = skipPart2;
}