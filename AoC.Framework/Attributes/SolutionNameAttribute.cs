namespace AoC.Framework.Attributes;

[AttributeUsage(AttributeTargets.Class)]
public class SolutionNameAttribute(string name) : Attribute
{
    public string Name { get; } = name;
}