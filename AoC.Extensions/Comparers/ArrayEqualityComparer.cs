namespace AoC.Extensions.Comparers;

public class ArrayEqualityComparer<T> : IEqualityComparer<T[]>
{
    public bool Equals(T[]? x, T[]? y)
    {
        if (x == null || y == null) 
            return x == y;

        return x.SequenceEqual(y);
    }

    public int GetHashCode(T[] obj)
    {
        unchecked
        {
            return obj.Aggregate(17, (current, item) => current * 31 + item!.GetHashCode());
        }
    }
}