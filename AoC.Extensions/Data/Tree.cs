using Spectre.Console;
using Spectre.Console.Rendering;

namespace AoC.Extensions.Data;

using System;
using System.Collections.Generic;
using System.Linq;

public class TreeNode<T>(T value)
{
    public T Value { get; set; } = value;
    public TreeNode<T>? Parent { get; private set; }
    public List<TreeNode<T>> Children { get; } = [];

    public TreeNode<T> AddChild(T value)
    {
        var treeNode = new TreeNode<T>(value);
        AddChild(treeNode);
        
        return treeNode;
    }

    public void AddChild(TreeNode<T> child)
    {
        ArgumentNullException.ThrowIfNull(child);
        
        child.Parent = this;
        Children.Add(child);
    }

    public bool RemoveChild(TreeNode<T> child)
    {
        ArgumentNullException.ThrowIfNull(child);
        
        child.Parent = null;
        return Children.Remove(child);
    }

    public TreeNode<T>? Find(T value) =>
        EqualityComparer<T>.Default.Equals(Value, value) ? this : Children.Select(child => child.Find(value)).OfType<TreeNode<T>>().FirstOrDefault();

    public IEnumerable<TreeNode<T>> IterateParents()
    {
        var current = this;
        
        while (true)
        {
            current = current.Parent;
            if (current == null)
                break;
            
            yield return current;
        }
    }
}

public class Tree<T>(T rootValue)
{
    public TreeNode<T> Root { get; } = new(rootValue);

    public void AddNode(T parentValue, T value)
    {
        var parentNode = Root.Find(parentValue);
        if (parentNode == null)
            throw new ArgumentException("Parent node not found.");
        
        parentNode.AddChild(new TreeNode<T>(value));
    }

    public TreeNode<T>? FindNode(T value) =>
        Root.Find(value);

    public int GetHeight() =>
        GetHeight(Root);

    private int GetHeight(TreeNode<T>? node)
    {
        if (node == null)
            return 0;
        
        if (node.Children.Count == 0)
            return 1;

        return 1 + node.Children.Max(GetHeight);
    }

    public int GetDepth(T value)
    {
        var node = FindNode(value);
        var depth = 0;
        
        while (node?.Parent != null)
        {
            node = node.Parent;
            depth++;
        }

        return depth;
    }

    public void TraversePreOrder(Action<T> action) =>
        TraversePreOrder(Root, action);

    private void TraversePreOrder(TreeNode<T> node, Action<T> action)
    {
        action(node.Value);
        
        foreach (var child in node.Children)
            TraversePreOrder(child, action);
    }

    public void TraversePostOrder(Action<T> action) =>
        TraversePostOrder(Root, action);

    private void TraversePostOrder(TreeNode<T> node, Action<T> action)
    {
        foreach (var child in node.Children)
            TraversePostOrder(child, action);

        action(node.Value);
    }

    public void TraverseBreadthFirst(Action<T> action)
    {
        var queue = new Queue<TreeNode<T>>();
        queue.Enqueue(Root);

        while (queue.Count != 0)
        {
            var current = queue.Dequeue();
            action(current.Value);
            foreach (var child in current.Children)
                queue.Enqueue(child);
        }
    }
    
    public IEnumerable<TreeNode<T>> TraverseBreadthFirst()
    {
        var queue = new Queue<TreeNode<T>>();
        queue.Enqueue(Root);

        while (queue.Count != 0)
        {
            var current = queue.Dequeue();
            yield return current;
            
            foreach (var child in current.Children)
                queue.Enqueue(child);
        }
    }

    public bool RemoveNode(T value)
    {
        if (EqualityComparer<T>.Default.Equals(Root.Value, value))
            throw new ArgumentException("Cannot remove root node.", nameof(value));

        var node = FindNode(value);
        if (node == null)
            return false;

        var parentNode = node.Parent;
        return parentNode?.RemoveChild(node) ?? false;
    }
    
    public Tree ToSpectreTree(Func<TreeNode<T>, IRenderable> nodeToRenderable)
    {
        var spectreTree = new Tree(nodeToRenderable(Root));
        AddChildrenToSpectreTree(Root, spectreTree, nodeToRenderable);
        
        return spectreTree;
    }

    private static void AddChildrenToSpectreTree(TreeNode<T> node, IHasTreeNodes spectreNode, Func<TreeNode<T>, IRenderable> nodeToRenderable)
    {
        foreach (var child in node.Children)
        {
            var childSpectreNode = spectreNode.AddNode(nodeToRenderable(child));

            AddChildrenToSpectreTree(child, childSpectreNode, nodeToRenderable);
        }
    }
}