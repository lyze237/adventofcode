using AoC.Extensions.Data;

namespace AoC.Extensions.Extensions;

public static class CharExtensions
{
    public static Direction DirectionFromArrow(this char c) =>
        c switch
        {
            '^' => Direction.Up,
            'v' => Direction.Down,
            '<' => Direction.Left,
            '>' => Direction.Right,
            _ => throw new ArgumentException("Invalid arrow direction")
        };
}