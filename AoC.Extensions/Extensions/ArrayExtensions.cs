﻿using AoC.Extensions.Data;

namespace AoC.Extensions.Extensions;

public static class ArrayExtensions
{
    public static void Deconstruct<T>(this IList<T?> list, out T? first, out T? second)
    {
        first = list.Count > 0 ? list[0] : default; // or throw
        second = list.Count > 1 ? list[1] : default; // or throw
    }

    public static void Deconstruct<T>(this IList<T?> list, out T? first, out T? second, out T? third)
    {
        first = list.Count > 0 ? list[0] : default; // or throw
        second = list.Count > 1 ? list[1] : default; // or throw
        third = list.Count > 2 ? list[2] : default; // or throw
    }

    public static T[][] Rotate<T>(this T[][] arr)
    {
        var width = arr[0].Length;
        var depth = arr.Length;

        var result = new T[width][];
        for (var i = 0; i < width; i++)
            result[i] = new T[depth];

        for (var i = 0; i < depth; i++)
        {
            for (var j = 0; j < width; j++)
            {
                result[j][depth - i - 1] = arr[i][j];
            }
        }

        return result;
    }

    public static void Print<T>(this IEnumerable<T[]> array)
    {
        foreach (var line in array)
            Console.WriteLine(string.Join("", line));

        Console.WriteLine();
    }

    public static T Get<T>(this T[][] arr, Point point) => point.Get(arr);
    public static bool InRectangle<T>(this T[][] arr, Point point) => point.InRectangle(arr);
    public static T Set<T>(this T[][] arr, Point point, T value) => point.Set(arr, value);

    public static int IndexOf<T>(this T[] array, T value)
    {
        for (var i = 0; i < array.LongLength; i++)
            if (Equals(array[i], value))
                return i;

        return -1;
    }
    
    public static Point IndexOf<T>(this T[][] array, T value)
    {
        foreach (var (x, y, item) in array.Iterate())
            if (Equals(item, value))
                return new Point(x, y);

        return new Point(-1, -1);
    }

    public static int LastIndexOf<T>(this T[] array, T value)
    {
        for (var i = array.Length - 1; i >= 0; i--)
            if (Equals(array[i], value))
                return i;

        return -1;
    }
    
    public static T[][] CloneArray<T>(this T[][] input)
    {
        var clone = new T[input.Length][];
        
        for (var i = 0; i < input.Length; i++)
            clone[i] = (T[]) input[i].Clone();

        return clone;
    }

    public static IEnumerable<(int x, int y, T item)> Iterate<T>(this T[][] array)
    {
        for (var y = 0; y < array.Length; y++)
        {
            for (var x = 0; x < array[y].Length; x++)
            {
                yield return (x, y, array[y][x]);
            }
        }
    }

    public static void ForEach<T>(this T[] array, Action<T> action)
    {
        foreach (var item in array)
            action(item);
    }
}