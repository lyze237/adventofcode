﻿namespace AoC.Year2023.Day05;

public class Solution : Day<(ulong[] seeds, Solution.Map[] maps)>
{
    public record Map(string From, string To)
    {
        public List<Mapping> Mapping { get; init; } = [];
    }

    public record Mapping(ulong DestRangeStart, ulong SourceRangeStart, ulong RangeLength);

    protected override object DoPart1((ulong[] seeds, Map[] maps) input) =>
        input.seeds.AsParallel()
            .Aggregate(ulong.MaxValue, (current, seed) => Math.Min(current, CurrentItem(input, seed)));

    protected override object DoPart2((ulong[] seeds, Map[] maps) input)
    {
        var ranges = new List<(ulong from, ulong to)>();
        for (var i = 0; i < input.seeds.Length; i += 2)
            ranges.Add((input.seeds[i], input.seeds[i] + input.seeds[i + 1]));
        
        ProgressReporter?.SetSteps(ranges.Count);
        
        return ranges.AsParallel().Aggregate(ulong.MaxValue,
            (current, seed) =>
            {
                ProgressReporter?.Step();
                return EnumerableExtensions.CreateRange(seed.from, seed.to)
                    .Min(i => Math.Min(current, CurrentItem(input, i)));
            });
    }

    private static ulong CurrentItem((ulong[] seeds, Map[] maps) input, ulong seed)
    {
        var currentItem = seed;

        foreach (var map in input.maps)
        {
            foreach (var (destRangeStart, sourceRangeStart, rangeLength) in map.Mapping)
            {
                if (currentItem >= sourceRangeStart && currentItem <= sourceRangeStart + rangeLength)
                {
                    currentItem = destRangeStart + (currentItem - sourceRangeStart);
                    break;
                }
            }
        }

        return currentItem;
    }

    protected override (ulong[] seeds, Map[] maps) ParseInputPart1(string input)
    {
        var lines = input.Split("\n");
        var seeds = Regex.Matches(lines[0], @"\d+").Select(s => s.Value.ToULong()).ToArray();

        var maps = new List<Map>();
        Map? currentMap = null;
        foreach (var line in lines)
        {
            var match = Regex.Match(line, @"(?<from>\w+)-to-(?<to>\w+) map:");
            if (maps.Count == 0 && !match.Success)
                continue;

            if (match.Success)
            {
                maps.Add(currentMap = new Map(match.Groups["from"].Value, match.Groups["to"].Value));
                continue;
            }

            if (string.IsNullOrWhiteSpace(line))
                continue;

            var (from, to, range) = line.Split(" ");
            currentMap!.Mapping.Add(new Mapping(from!.ToULong(), to!.ToULong(), range!.ToULong()));
        }

        return (seeds, maps.ToArray());
    }
}