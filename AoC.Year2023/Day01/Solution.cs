﻿namespace AoC.Year2023.Day01;

public class Solution : Day<string[]>
{
    private static readonly Dictionary<string, int> Numbers = new()
    {
        { "one", 1 }, { "two", 2 }, { "three", 3 }, { "four", 4 }, { "five", 5 }, { "six", 6 }, { "seven", 7 },
        { "eight", 8 }, { "nine", 9 }
    };

    protected override object DoPart1(string[] input) =>
        Run(input, @"\d");

    protected override object DoPart2(string[] input) =>
        Run(input, @$"{string.Join("|", Numbers.Select(n => n.Key))}|\d");

    private static int Run(IEnumerable<string> input, string regex) =>
        input.Select(line =>
            ConvertMatch(Regex.Match(line, regex).Value) * 10 +
            ConvertMatch(Regex.Match(line, regex, RegexOptions.RightToLeft).Value)).Sum();

    private static int ConvertMatch(string str) =>
        int.TryParse(str, out var number) ? number : Numbers[str];

    protected override string[] ParseInputPart1(string input) =>
        input.Split("\n");
}