﻿namespace AoC.Year2023.Day03;

public class Solution : Day<Solution.Part[]>
{
    public record Symbol(char Type, Point Point);

    public record Part(int Number, Symbol Symbol);

    protected override object DoPart1(Part[] input) =>
        input.Sum(part => part.Number);

    protected override object DoPart2(Part[] input) =>
        input
            .Where(part => part.Symbol.Type == '*')
            .GroupBy(part => part.Symbol.Point)
            .Where(group => group.Count() == 2)
            .Sum(group => group.Select(g => g.Number).Mul());

    protected override Part[] ParseInputPart1(string inputString)
    {
        var input = inputString.Split("\n");

        var parts = new List<Part>();
        for (var i = 0; i < input.Length; i++)
        {
            var matches = Regex.Matches(input[i], @"(\d+)");
            foreach (Match match in matches)
            {
                var symbol = IsAdjacentToSymbol(match, i, input);
                if (symbol != null)
                    parts.Add(new Part(match.Value.ToInt(), symbol));
            }
        }

        return parts.ToArray();
    }

    private static Symbol? IsAdjacentToSymbol(Match match, int i, string[] input)
    {
        for (var x = match.Index - 1; x <= match.Index + match.Length; x++)
        {
            for (var y = i - 1; y <= i + 1; y++)
            {
                if (x < 0 || y < 0 || y >= input.Length || x >= input[i].Length)
                    continue;

                var c = input[y][x];
                if (char.IsDigit(c) || c == '.')
                    continue;

                return new Symbol(c, new Point(y, x));
            }
        }

        return null;
    }
}