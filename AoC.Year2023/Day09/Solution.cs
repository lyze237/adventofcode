﻿namespace AoC.Year2023.Day09;

public class Solution : Day<long[][]>
{
    protected override object DoPart1(long[][] input) =>
        input.Select(Solve).Sum();

    protected override object DoPart2(long[][] input) =>
        input.Select(i => i.Reverse().ToArray()).Select(Solve).Sum();

    private static long Solve(long[] line) =>
        !line.Any() ? 0 : Solve(FindNextValues(line)) + line.Last();

    private static long[] FindNextValues(long[] line) =>
        line.Zip(line.Skip(1)).Select(l => l.Second - l.First).ToArray();

    protected override long[][] ParseInputPart1(string input) =>
        input
            .Split("\n")
            .Select(line => line.Split(" ").Select(i => i.ToLong()).ToArray())
            .ToArray();
}