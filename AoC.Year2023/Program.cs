﻿using System.Reflection;

await new AocBuilder()
    .SetYear(2023)
    .SetDays(Enumerable.Range(1, 18).ToArray())
    .SetLogLevel(Logger.LogLevel.Solve)
    .SkipExamples()
    .Execute(Assembly.GetAssembly(typeof(Program))!);
