﻿namespace AoC.Year2023.Day08;

public class Solution : Day<(char[] directions, Solution.Node[] nodes)>
{
    public record Node(string Name, string Left, string Right);

    protected override object DoPart1((char[] directions, Node[] nodes) input) =>
        FindZNode("AAA", input, true);

    protected override object DoPart2((char[] directions, Node[] nodes) input) =>
        input.nodes
            .Where(n => n.Name.Contains('A'))
            .AsParallel()
            .Select(c => FindZNode(c.Name, input, false))
            .FindLcm();

    private static int FindZNode(string startNode, (char[] directions, Node[] nodes) input, bool allThree)
    {
        var steps = 0;

        var currentNode = startNode;
        foreach (var direction in input.directions.RepeatIndefinitely())
        {
            var node = input.nodes.First(n => n.Name == currentNode);
            currentNode = direction switch
            {
                'L' => node.Left,
                'R' => node.Right,
                _ => throw new ArgumentOutOfRangeException(nameof(direction))
            };

            steps++;

            if (allThree ? currentNode == "ZZZ" : currentNode.Contains('Z'))
                return steps;
        }

        return 0;
    }

    protected override (char[] directions, Node[] nodes) ParseInputPart1(string input)
    {
        var lines = input.Split("\n");
        var directions = lines[0].ToCharArray();

        var nodes = lines
            .Skip(2)
            .Select(line => Regex.Match(line, @"(?<name>\w+) = \((?<left>\w+), (?<right>\w+)\)"))
            .Select(match =>
                new Node(match.Groups["name"].Value, match.Groups["left"].Value, match.Groups["right"].Value))
            .ToArray();

        return (directions, nodes);
    }
}