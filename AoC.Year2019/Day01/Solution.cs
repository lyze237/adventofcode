using AoC.Framework.Attributes;

namespace AoC.Year2019.Day01;

[SolutionName("The Tyranny of the Rocket Equation")]
public class Solution : Day<int[]>
{
    protected override object DoPart1(int[] input)
    {
        ProgressReporter?.SetSteps(input.Length);

        var sum = 0;
        foreach (var weight in input)
        {
            sum += weight / 3 - 2;

            ProgressReporter?.Step();
        }

        return sum;
    }

    protected override object DoPart2(int[] input)
    {
        ProgressReporter?.SetSteps(input.Length);

        long sum = 0;
        foreach (var weight in input)
        {
            sum += CalculateFuel(weight);

            ProgressReporter?.Step();
        }

        return sum;
    }

    private static long CalculateFuel(int weight)
    {
        var fuel = weight / 3 - 2;
        if (fuel <= 0)
            return 0;

        return fuel + CalculateFuel(fuel);
    }

    protected override int[] ParseInputPart1(string input) =>
        input.Split("\n").Select(int.Parse).ToArray();
}