using AoC.Framework.Attributes;

namespace AoC.Year2019.Day06;

[SolutionName("Universal Orbit Map")]
public class Solution : Day<(string left, string right)[]>
{
    protected override object DoPart1((string left, string right)[] input) =>
        BuildTree(input)
            .TraverseBreadthFirst()
            .Sum(node => node.IterateParents().Count());

    protected override object DoPart2((string left, string right)[] input) =>
        BreadthFirstSearch("YOU", "SAN", BuildTree(input)) - 2;

    private static int BreadthFirstSearch(string start, string end, Tree<string> tree)
    {
        var best = int.MaxValue;
        var visited = new HashSet<string>();
        var queue = new Queue<(TreeNode<string> node, int transfers)>();

        queue.Enqueue((tree.FindNode(start)!, 0));
        while (queue.Count > 0)
        {
            var (node, transfers) = queue.Dequeue();

            if (node.Value == end && transfers < best)
            {
                best = transfers;
                continue;
            }
            
            if (!visited.Add(node.Value))
                continue;

            var neighbours = node.Children
                .Select(child => (child, transfers + 1))
                .ToList();
            if (node.Parent != null)
                neighbours.Add((node.Parent, transfers + 1));
            
            foreach (var neighbour in neighbours)
                queue.Enqueue(neighbour);
        }

        return best;
    }

    private Tree<string> BuildTree((string left, string right)[] input)
    {
        var tree = new Tree<string>("COM");
        var queue = new Queue<(string left, string right)>(input);
        ProgressReporter?.SetSteps(queue.Count);

        while (queue.Count != 0)
        {
            var (left, right) = queue.Dequeue();

            var leftNode = tree.FindNode(left);
            if (leftNode == null)
            {
                queue.Enqueue((left, right));
            }
            else
            {
                leftNode.AddChild(right);
                ProgressReporter?.Step();
            }
        }

        return tree;
    }

    protected override (string left, string right)[] ParseInputPart1(string input) =>
        input.Split("\n").Select(line =>
        {
            var (left, right) = line.Split(")");
            return (left!, right!);
        }).ToArray();
}