namespace AoC.Year2019.Day05;

public partial class Solution
{
    /// <summary>
    /// An IntCode Computer.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Parameters are defined with $
    /// Input, Output, Pointer are special registers
    /// Literals do not have a suffix
    /// </para>
    ///
    /// <para>
    /// <list type="table">
    /// <listheader>
    ///     <term>Opcode</term>
    ///     <description>Description</description> 
    /// </listheader>
    /// <item>
    ///     <term>1</term>
    ///     <description>$2 = $0 + $1</description>
    /// </item>
    /// <item>
    ///     <term>2</term>
    ///     <description>$2 = $0 * $1</description>
    /// </item>
    /// <item>
    ///     <term>3</term>
    ///     <description>$0 = Input</description>
    /// </item>
    /// <item>
    ///     <term>4</term>
    ///     <description>Output = $0</description>
    /// </item>
    /// <item>
    ///     <term>5</term>
    ///     <description>if ($0 != 0) Pointer = $1</description>
    /// </item>
    /// <item>
    ///     <term>6</term>
    ///     <description>if ($0 == 0) Pointer = $1</description>
    /// </item>
    /// <item>
    ///     <term>7</term>
    ///     <description>if ($0 &lt; $1) $2 = 1 else $2 = 0</description>
    /// </item>
    /// <item>
    ///     <term>8</term>
    ///     <description>if ($0 == $1) $2 = 1 else $2 = 0</description>
    /// </item>
    /// </list>
    /// </para>
    /// </remarks>
    private sealed class IntCode(int[] instructions, Queue<int> input)
    {
        private int instructionPointer;
        private readonly List<int> output = [];

        public List<int> Run()
        {
            while (instructionPointer < instructions.Length)
            {
                var oldInstructionPointer = instructionPointer;

                if (ProcessInstruction())
                    break;

                if (instructionPointer == oldInstructionPointer)
                    throw new ArgumentException("Infinite loop detected");
            }

            return output;
        }

        private bool ProcessInstruction()
        {
            var instruction = instructions[instructionPointer];
            var (opcode, parameterModes) = ExtractInstruction(instruction);

            switch (opcode)
            {
                case 1:
                    var a = GetParameterValue(parameterModes, 0);
                    var b = GetParameterValue(parameterModes, 1);
                    
                    SetParameterValue(parameterModes, 2, a + b);

                    instructionPointer += 4;
                    break;
                case 2:
                    a = GetParameterValue(parameterModes, 0);
                    b = GetParameterValue(parameterModes, 1);

                    SetParameterValue(parameterModes, 2, a * b);

                    instructionPointer += 4;
                    break;
                case 3:
                    var i = input.Dequeue();
                    SetParameterValue(parameterModes, 0, i);

                    instructionPointer += 2;
                    break;
                case 4:
                    a = GetParameterValue(parameterModes, 0);
                    output.Add(a);

                    instructionPointer += 2;
                    break;
                case 5:
                    a = GetParameterValue(parameterModes, 0);
                    b = GetParameterValue(parameterModes, 1);
                    
                    if (a != 0)
                        instructionPointer = b;
                    else
                        instructionPointer += 3;
                    break;
                case 6:
                    a = GetParameterValue(parameterModes, 0);
                    b = GetParameterValue(parameterModes, 1);
                    
                    if (a == 0)
                        instructionPointer = b;
                    else
                        instructionPointer += 3;
                    break;
                case 7:
                    a = GetParameterValue(parameterModes, 0);
                    b = GetParameterValue(parameterModes, 1);
                    
                    SetParameterValue(parameterModes, 2, a < b ? 1 : 0);

                    instructionPointer += 4;
                    break;
                case 8:
                    a = GetParameterValue(parameterModes, 0);
                    b = GetParameterValue(parameterModes, 1);
                    
                    SetParameterValue(parameterModes, 2, a == b ? 1 : 0);

                    instructionPointer += 4;
                    break;
                case 99:
                    return true;
                default:
                    throw new ArgumentOutOfRangeException(nameof(opcode), $"Invalid opcode: {opcode}");
            }

            return false;
        }

        private static (int opcode, int[] parameterModes) ExtractInstruction(int instruction)
        {
            var opcode = instruction % 100;
            var parameters = (instruction / 100)
                .ToString()
                .Reverse()
                .Select(num => num.ToInt()).ToArray();

            return (opcode, parameters);
        }

        private void SetParameterValue(int[] parameters, int index, int value) =>
            instructions[GetParameterIndex(parameters, index)] = value;

        private int GetParameterValue(int[] parameters, int index) =>
            instructions[GetParameterIndex(parameters, index)];

        private int GetParameterIndex(int[] parameters, int index)
        {
            var mode = index < parameters.Length ? parameters[index] : 0;
            var parameterPointer = instructionPointer + 1 + index;

            return mode switch
            {
                0 => instructions[parameterPointer],
                1 => parameterPointer,
                _ => throw new ArgumentOutOfRangeException(nameof(mode), $"Invalid parameter mode: {mode}")
            };
        }
    }
}