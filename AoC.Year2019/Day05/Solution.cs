using AoC.Framework.Attributes;

namespace AoC.Year2019.Day05;

[SolutionName("Sunny with a Chance of Asteroids")]
[SolutionSkipExamples]
public partial class Solution : Day<int[]>
{
    protected override object DoPart1(int[] input) =>
        new IntCode(input, new Queue<int>([1])).Run()[^1];

    protected override object DoPart2(int[] input) =>
        new IntCode(input, new Queue<int>([5])).Run()[^1];

    protected override int[] ParseInputPart1(string input) =>
        input.Split(",").Select(num => num.ToInt()).ToArray();
}
