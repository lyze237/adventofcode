using AoC.Framework.Attributes;

namespace AoC.Year2019.Day04;

[SolutionName("Secure Container")]
[SolutionSkipExamples]
public class Solution : Day<(int from, int to)>
{
    protected override object DoPart1((int from, int to) input) => 
        Calculate(input.from, input.to, count => count < 2);

    protected override object DoPart2((int from, int to) input) => 
        Calculate(input.from, input.to, count => count != 2);

    private static int Calculate(int from, int to, Func<int, bool> func) =>
        Enumerable.Range(from, to - from)
            .Count(number => IsValidCode(number, func));

    private static bool IsValidCode(int number, Func<int, bool> func)
    {
        var charArray = number.ToString().ToCharArray();
        if (charArray.GroupBy(c => c).All(g => func(g.Count())))
            return false;

        return charArray
            .OrderBy(c => c)
            .SequenceEqual(charArray);
    }

    protected override (int from, int to) ParseInputPart1(string input)
    {
        var (fromStr, toStr) = input.Split("-");
        return (fromStr!.ToInt(), toStr!.ToInt());
    }
}