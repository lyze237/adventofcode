using AoC.Framework.Attributes;

namespace AoC.Year2019.Day03;

[SolutionName("Crossed Wires")]
public partial class Solution : Day<Solution.DirectionLength[][]>
{
    public record DirectionLength(Direction Direction, int Length)
    {
        public DirectionLength(Match match) : this(match.Groups["dir"].Value.ToDirection(), match.Groups["length"].Value.ToInt())
        {
        }
    }

    protected override object DoPart1(DirectionLength[][] input)
    {
        var startPoint = new Point(0, 0);

        var wire1 = CalculateWire(startPoint, input[0]);
        var wire2 = CalculateWire(startPoint, input[1]);

        var intersections = wire1
            .Where(point => wire2.ContainsKey(point.Key))
            .Select(point => (point: point.Key, distance: point.Key.ManhattanDistance(startPoint)))
            .OrderBy(point => point.distance)
            .First();
        
        return intersections.distance;
    }
    
    protected override object DoPart2(DirectionLength[][] input)
    {
        var startPoint = new Point(0, 0);

        var wire1 = CalculateWire(startPoint, input[0]);
        var wire2 = CalculateWire(startPoint, input[1]);

        var intersections = wire1
            .Where(point => wire2.ContainsKey(point.Key))
            .Select(point => (point: point.Key, distance: wire1[point.Key] + wire2[point.Key]))
            .OrderBy(point => point.distance)
            .First();
        
        return intersections.distance;
    }

    private static Dictionary<Point, int> CalculateWire(Point startPoint, DirectionLength[] wire)
    {
        var points = new Dictionary<Point, int>();

        var point = startPoint;
        var distance = 0;
        foreach (var (direction, length) in wire)
        {
            for (var i = 0; i < length; i++)
            {
                (point, distance) = (point.Move(direction), distance + 1);
                points.TryAdd(point, distance);
            }
        }

        return points;
    }

    protected override DirectionLength[][] ParseInputPart1(string input)
    {
        return input.Split("\n")
            .Select(line => DirectionRegex().Matches(line))
            .Select(matches => matches.Select(match => new DirectionLength(match)).ToArray())
            .ToArray();
    }

    [GeneratedRegex("(?<dir>[UDLR])(?<length>\\d+),?")]
    private static partial Regex DirectionRegex();
}