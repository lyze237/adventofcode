using AoC.Framework.Attributes;

namespace AoC.Year2019.Day02;

[SolutionName("1202 Program Alarm")]
public class Solution : Day<int[]>
{
    protected override object DoExamplePart1(int[] input) => 
        RunIntCode(input);

    protected override object DoExamplePart2(int[] input) => 
        0;

    protected override object DoPart1(int[] input)
    {
        input[1] = 12;
        input[2] = 2;
        
        return RunIntCode(input);
    }
    
    protected override object DoPart2(int[] input)
    {
        for (var noun = 0; noun <= 99; noun++)
        {
            for (var verb = 0; verb <= 99; verb++)
            {
                var tmpInput = input.ToArray();
                
                tmpInput[1] = noun;
                tmpInput[2] = verb;
                
                if (RunIntCode(tmpInput) == 19690720)
                    return 100 * noun + verb;
            }
        }

        return -1;
    }

    private static int RunIntCode(int[] input)
    {
        var counter = 0;
        
        while (true)
        {
            var opcode = input[counter];
            if (opcode == 1)
            {
                var a = input[input[counter + 1]];
                var b = input[input[counter + 2]];
                var position = input[counter + 3];

                input[position] = a + b;
                counter += 4;
            }
            else if (opcode == 2)
            {
                var a = input[input[counter + 1]];
                var b = input[input[counter + 2]];
                var position = input[counter + 3];

                input[position] = a * b;
                counter += 4;
            }
            else if (opcode == 99)
            {
                break;
            }
        }

        return input[0];
    }

    protected override int[] ParseInputPart1(string input) =>
        input.Split(",").Select(int.Parse).ToArray();
}