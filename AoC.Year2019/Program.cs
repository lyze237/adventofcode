﻿using System.Reflection;

await new AocBuilder()
    .SetYear(2019)
    .SetDays(6)
    .SetLogLevel(Logger.LogLevel.Solve)
    //.SetInteractive()
    .Execute(Assembly.GetAssembly(typeof(Program))!);
