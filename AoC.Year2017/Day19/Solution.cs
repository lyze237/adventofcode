﻿namespace AoC.Year2017.Day19;

public class Solution : Day<string[]>
{
    protected override object DoPart1(string[] input)
    {
        // find starting position

        var direction = Direction.Down;
        var position = new Point(input[0].IndexOf('|'), 0);

        var visited = "";

        try
        {
            while (true)
            {
                var c = input[position.Y][(int) position.X];
                switch (c)
                {
                    case '+':
                        if (input[position.Y - 1][(int) position.X] != ' ' && direction != Direction.Down)
                        {
                            position = position with { Y = position.Y - 1 };
                            direction = Direction.Up;
                        }
                        else if (input[position.Y + 1][(int) position.X] != ' ' && direction != Direction.Up)
                        {
                            position = position with { Y = position.Y + 1 };
                            direction = Direction.Down;
                        }
                        else if (input[position.Y][(int) position.X - 1] != ' ' && direction != Direction.Right)
                        {
                            position = position with { X = position.X - 1 };
                            direction = Direction.Left;
                        }
                        else if (input[position.Y][(int) position.X + 1] != ' ' && direction != Direction.Left)
                        {
                            position = position with { X = position.X + 1 };
                            direction = Direction.Right;
                        }

                        break;
                    case ' ':
                        return visited;
                    default:
                        if (c is >= 'A' and <= 'Z')
                            visited += c;

                        Console.WriteLine($"Moving {direction}");
                        switch (direction)
                        {
                            case Direction.Down:
                                position = position with { Y = position.Y + 1 };
                                break;
                            case Direction.Up:
                                position = position with { Y = position.Y - 1 };
                                break;
                            case Direction.Left:
                                position = position with { X = position.X - 1 };
                                break;
                            case Direction.Right:
                                position = position with { X = position.X + 1 };
                                break;
                        }

                        break;
                }
            }
        }
        catch (IndexOutOfRangeException)
        {
            return visited;
        }
    }

    protected override object DoPart2(string[] input)
    {
        var direction = Direction.Down;
        var position = new Point(input[0].IndexOf('|'), 0);

        for (var steps = 0;; steps++)
        {
            var c = input[position.Y][(int) position.X];
            switch (c)
            {
                case '+':
                    if (input[position.Y - 1][(int) position.X] != ' ' && direction != Direction.Down)
                    {
                        position = position with { Y = position.Y - 1 };
                        direction = Direction.Up;
                    }
                    else if (input[position.Y + 1][(int) position.X] != ' ' && direction != Direction.Up)
                    {
                        position = position with { Y = position.Y + 1 };
                        direction = Direction.Down;
                    }
                    else if (input[position.Y][(int) position.X - 1] != ' ' && direction != Direction.Right)
                    {
                        position = position with { X = position.X - 1 };
                        direction = Direction.Left;
                    }
                    else if (input[position.Y][(int) position.X + 1] != ' ' && direction != Direction.Left)
                    {
                        position = position with { X = position.X + 1 };
                        direction = Direction.Right;
                    }

                    break;
                case ' ':
                    return steps;
                default:
                    switch (direction)
                    {
                        case Direction.Down:
                            position = position with { Y = position.Y + 1 };
                            break;
                        case Direction.Up:
                            position = position with { Y = position.Y - 1 };
                            break;
                        case Direction.Left:
                            position = position with { X = position.X - 1 };
                            break;
                        case Direction.Right:
                            position = position with { X = position.X + 1 };
                            break;
                    }

                    break;
            }
        }
    }
    
    protected override string[] ParseInputPart1(string input) => 
        input.Split("\n");
}