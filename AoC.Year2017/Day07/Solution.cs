﻿namespace AoC.Year2017.Day07;

public class Solution : Day<Solution.Disk[]>
{
    protected override object DoPart1(Disk[] input)
    {
        input.ForEach(disk => disk.AddChildDisks(input));

        return GetBaseDisk(input).Name;
    }

    protected override object DoPart2(Disk[] input)
    {
        input.ForEach(d => d.AddChildDisks(input));

        var disk = GetBaseDisk(input);
        var targetWeight = 0;

        while (!disk.IsBalanced())
            (disk, targetWeight) = disk.GetUnbalancedChild();

        var weightDiff = targetWeight - disk.GetTotalWeight();

        return disk.Weight + weightDiff;
    }

    private Disk GetBaseDisk(Disk[] disks)
    {
        var disk = disks[0];
        while (disk.Parent != null)
            disk = disk.Parent;
        
        return disk;
    }

    public class Disk
    {
        public int Weight { get; set; }
        public string Name { get; set; }
        public List<string> ChildNames { get; set; }
        public List<Disk> ChildDisks { get; set; }
        public Disk? Parent { get; set; }

        private static readonly Regex InputRegex = new(@"(\w*) \((\d*)\)( -> ([\w, ]*))?");

        public Disk(string input)
        {
            var words = InputRegex.Match(input);

            Name = words.Groups[1].Value;
            Weight = Convert.ToInt32(words.Groups[2].Value);

            ChildNames = words.Groups[4].Value.Split(',').Where(s => !string.IsNullOrEmpty(s)).Select(s => s.Trim())
                .ToList();
        }

        public void AddChildDisks(Disk[] disks)
        {
            try
            {
                ChildDisks = ChildNames.Select(name => disks.First(disc => disc.Name == name)).ToList();
                ChildDisks.ForEach(child => child.Parent = this);
            }
            catch
            {
                // ignored
            }
        }

        public int GetTotalWeight()
        {
            return ChildDisks.Sum(disk => disk.GetTotalWeight()) + Weight;
        }

        public bool IsBalanced()
        {
            return ChildDisks.GroupBy(disk => disk.GetTotalWeight()).Count() == 1;
        }

        public (Disk disk, int targetWeight) GetUnbalancedChild()
        {
            var groups = ChildDisks.GroupBy(disk => disk.GetTotalWeight());
            var targetWeight = groups.First(disk => disk.Count() > 1).Key;
            var unbalancedChild = groups.First(disk => disk.Count() == 1).First();

            return (unbalancedChild, targetWeight);
        }
    }

    protected override Disk[] ParseInputPart1(string input) => 
        input.Split("\n").Select(line => new Disk(line)).ToArray();
}