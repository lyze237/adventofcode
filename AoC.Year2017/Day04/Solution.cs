﻿namespace AoC.Year2017.Day04;

public class Solution : Day<string[]>
{
    protected override object DoPart1(string[] input)
    {
        return input
            .Select(line => line.Split(" "))
            .Count(wordsArray => new HashSet<string>(wordsArray).Count == wordsArray.Length);
    }

    protected override object DoPart2(string[] input)
    {
        var okLines = 0;
        
        foreach (var line in input)
        {
            var wordsArray = line.Split(" ");

            var foundSame = false;

            for (var i = 0; i < wordsArray.Length; i++)
            {
                var word = wordsArray[i];

                if (wordsArray.Where((_, j) => i != j).Any(otherWord => CheckAnagram(word, otherWord)))
                    foundSame = true;

                if (foundSame)
                    break;
            }

            if (!foundSame)
                okLines++;
        }

        return okLines;
    }

    private static bool CheckAnagram(string our, string other)
    {
        if (our == other)
            return true;

        if (our.Length != other.Length)
            return false;

        var ourArray = our.ToCharArray();
        var otherArray = other.ToCharArray();

        Array.Sort(ourArray);
        Array.Sort(otherArray);

        var result = new string(ourArray) == new string(otherArray);

        return result;
    }

    protected override string[] ParseInputPart1(string input) => 
        input.Split("\n");
}