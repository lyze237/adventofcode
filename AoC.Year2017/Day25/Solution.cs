﻿namespace AoC.Year2017.Day25;

public partial class Solution : Day<string[]>
{
    protected override object DoPart1(string[] input)
    {
        List<Task> allTasks = [new MoveTask(), new WriteTask(), new ContinueTask()];

        List<State> states = [];
        State currentState;
            
        State? stateToProcess = null;
        var currentIf = 0;            
        foreach (var l in input)
        {
            var line = l.Trim();

            if (InStateRegex().IsMatch(line))
            {
                var name = InStateRegex().Match(line).Groups[1].Value;
                stateToProcess = new State(name);
                states.Add(stateToProcess);
            }
            else if (IfCurrentValueRegex().IsMatch(line))
            {
                currentIf = Convert.ToInt32(IfCurrentValueRegex().Match(line).Groups[1].Value);
            }
            else if (!string.IsNullOrEmpty(line))
            {
                foreach (var task in allTasks)
                {
                    if (task.Matches(line))
                    {
                        if (currentIf == 0)
                            stateToProcess?.If0Tasks.Add(line, task);
                        else
                            stateToProcess?.If1Tasks.Add(line, task);
                    }
                }
            }
        }

        currentState = states[0];

        var values = new Dictionary<int, int>();
        var cursor = 0;
        for (var i = 0; i < 12_261_543; i++)
        {
            var next = currentState.Process(values, ref cursor);
            currentState = states.Find(state => state.Name == next);
        }

        return values.Count(pair => pair.Value == 1);
    }

    protected override object DoPart2(string[] input)
    {
        return -1;
    }

    private class State(string name)
    {
        public string Name { get; set; } = name;

        public Dictionary<string, Task> If1Tasks { get; set; } = new();
        
        public Dictionary<string, Task> If0Tasks { get; set; } = new();

        public string Process(Dictionary<int, int> values, ref int cursor)
        {
            if (values.TryGetValue(cursor, out var value) && value == 1)
                return Process(values, ref cursor, If1Tasks);
            
            return Process(values, ref cursor, If0Tasks);
        }

        private string Process(Dictionary<int, int> values, ref int cursor, Dictionary<string, Task> tasks)
        {
            foreach (var task in tasks)
            {
                task.Value.Run(task.Key, values, ref cursor, out var nextState);

                if (nextState != null)
                    return nextState;
            }

            return null;
        }
    }
    
    private class ContinueTask() : Task(@"- Continue with state ([A-Z]+).")
    {
        public override void Run(string line, Dictionary<int, int> values, ref int cursor, out string nextState)
        {
            nextState = Regex.Match(line).Groups[1].Value;
        }
    }
    
    private class MoveTask() : Task(@"- Move one slot to the ([a-z]+).")
    {
        public override void Run(string line, Dictionary<int, int> values, ref int cursor, out string nextState)
        {
            if (Regex.Match(line).Groups[1].Value == "left")
                cursor--;
            else
                cursor++;
            
            nextState = null;
        }
    }

    private class WriteTask() : Task(@"- Write the value (\d).")
    {
        public override void Run(string line, Dictionary<int, int> values, ref int cursor, out string nextState)
        {
            var valueToWrite = Convert.ToInt32(Regex.Match(line).Groups[1].Value);
            values[cursor] = valueToWrite;
            
            nextState = null;
        }
    }
    
    private abstract class Task(string regex)
    {
        public Regex Regex { get; set; } = new(regex);

        public abstract void Run(string line, Dictionary<int, int> values, ref int cursor, out string nextState);

        public bool Matches(string line)
        {
            return Regex.IsMatch(line);
        }
    }

    protected override string[] ParseInputPart1(string input) => 
        input.Split("\n");
    [GeneratedRegex(@"In state (\w+):")]
    private static partial Regex InStateRegex();
    [GeneratedRegex(@"If the current value is (\d):")]
    private static partial Regex IfCurrentValueRegex();
}