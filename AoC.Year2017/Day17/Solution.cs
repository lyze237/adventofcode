﻿namespace AoC.Year2017.Day17;

public class Solution : Day<int>
{
    protected override object DoPart1(int input)
    {
        var buffer = new List<int> { 0 };

        var currentPosition = 0;

        for (var i = 1; i <= 2017; i++)
        {
            currentPosition = (currentPosition + input % buffer.Count) % buffer.Count;
            buffer.Insert(++currentPosition, i);
        }

        return buffer[buffer.IndexOf(2017) + 1];
    }

    protected override object DoPart2(int input)
    {
        var currentPosition = 0;
        var result = 0;

        for (var i = 0; i < 50_000_000; i++)
        {
            currentPosition = (currentPosition + input % (i + 1)) % (i + 1);
            if (currentPosition++ == 0)
            {
                result = i + 1;
            }
        }

        return result;
    }

    protected override int ParseInputPart1(string input) => 
        input.ToInt();
}