﻿namespace AoC.Year2017.Day13;

public class Solution : Day<string[]>
{
    protected override object DoPart1(string[] input)
    {
        var layers = input
            .Select(line => line.Split(':'))
            .Select(splitted =>
                new Layer(Convert.ToInt32(splitted[0].Trim()), Convert.ToInt32(splitted[1].Trim()))).ToList();

        var caughtAmount = 0;

        var maxLayerCount = layers.Max(l => l.Position);
        for (var i = 0; i <= maxLayerCount; i++)
        {
            var layer = layers.FirstOrDefault(l => l.Position == i);
            if (layer?.IsCaught(i) == true)
            {
                caughtAmount += layer.GetSeverity();
            }
        }
            
        return caughtAmount;
    }

    protected override object DoPart2(string[] input)
    {
        bool caught;
        var delay = 0;

        var layers = input
            .Select(line => line.Split(':'))
            .Select(splitted =>
                new Layer(Convert.ToInt32(splitted[0].Trim()), Convert.ToInt32(splitted[1].Trim()))).ToList();

        do
        {
            caught = false;          

            var maxLayerCount = layers.Max(l => l.Position);
                
            for (var i = 0; i <= maxLayerCount; i++)
            {
                var layer = layers.FirstOrDefault(l => l.Position == i);
                if (layer?.IsCaught(delay + i) == true)
                {
                    caught = true;
                    delay++;
                    break;
                }
            }
        } while (caught);
            
        return delay;
    }

    public class Layer
    {
        public int Position { get; }
        public int Depth { get; }

        private readonly int depthMultiply;
        
        public Layer(int position, int depth)
        {
            Position = position;
            Depth = depth;
            depthMultiply = (Depth - 1) * 2;
        }

        public bool IsCaught(int delay) => 
            (delay % depthMultiply) == 0;

        public int GetSeverity() => 
            Depth * Position;

        private bool Equals(Layer other) => 
            Position == other.Position;

        public override bool Equals(object? obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj.GetType() == this.GetType() && Equals((Layer) obj);
        }

        public override int GetHashCode() => 
            Position;

        public override string ToString() => 
            $"{nameof(Position)}: {Position}, {nameof(Depth)}: {Depth}";
    }


    protected override string[] ParseInputPart1(string input) => 
        input.Split("\n");
}