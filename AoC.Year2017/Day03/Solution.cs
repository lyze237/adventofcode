﻿namespace AoC.Year2017.Day03;

public class Solution : Day<int>
{
    protected override object DoPart1(int input)
    {
        var ring = 0;
        for (var maxRingVal = 1; maxRingVal < input; maxRingVal += 8 * ++ring) ;

        var sequenceIndex = (input - 1) % (2 * ring);
        var distAlongEdge = Math.Abs(sequenceIndex - ring);
        var taxiDist = distAlongEdge + ring;

        return taxiDist;
    }

    protected override object DoPart2(int input)
    {
        var grid = new Dictionary<Point, int>
        {
            [new Point(0, 0)] = 1
        };

        var direction = Direction.Right;
        var x = 0;
        var y = 0;

        var init = false;
        while (true)
        {
            var point = new Point(x, y);


            var val = 0;

            if (!init)
                init = true;
            else
                switch (direction)
                {
                    case Direction.Right:
                        if (!grid.ContainsKey(new Point(x, y + 1)))
                        {
                            direction = Direction.Up;
                        }

                        break;
                    case Direction.Left:
                        if (!grid.ContainsKey(new Point(x, y - 1)))
                        {
                            direction = Direction.Down;
                        }

                        break;
                    case Direction.Up:
                        if (!grid.ContainsKey(new Point(x - 1, y)))
                        {
                            direction = Direction.Left;
                        }

                        break;
                    case Direction.Down:
                        if (!grid.ContainsKey(new Point(x + 1, y)))
                        {
                            direction = Direction.Right;
                        }

                        break;
                }


            if (grid.ContainsKey(new Point(x - 1, y - 1)))
            {
                val += grid[new Point(x - 1, y - 1)];
            }

            if (grid.ContainsKey(new Point(x, y - 1)))
            {
                val += grid[new Point(x, y - 1)];
            }

            if (grid.ContainsKey(new Point(x + 1, y - 1)))
            {
                val += grid[new Point(x + 1, y - 1)];
            }


            if (grid.ContainsKey(new Point(x - 1, y)))
            {
                val += grid[new Point(x - 1, y)];
            }

            if (grid.ContainsKey(new Point(x, y)))
            {
                val += grid[new Point(x, y)];
            }

            if (grid.ContainsKey(new Point(x + 1, y)))
            {
                val += grid[new Point(x + 1, y)];
            }


            if (grid.ContainsKey(new Point(x - 1, y + 1)))
            {
                val += grid[new Point(x - 1, y + 1)];
            }

            if (grid.ContainsKey(new Point(x, y + 1)))
            {
                val += grid[new Point(x, y + 1)];
            }

            if (grid.ContainsKey(new Point(x + 1, y + 1)))
            {
                val += grid[new Point(x + 1, y + 1)];
            }

            grid[point] = val;
            Console.WriteLine(point + ": " + val + " (" + grid.Count + ")");

            if (val > input)
            {
                return val;
            }


            switch (direction)
            {
                case Direction.Right:
                    x++;
                    break;
                case Direction.Left:
                    x--;
                    break;
                case Direction.Up:
                    y++;
                    break;
                case Direction.Down:
                    y--;
                    break;
            }
        }
    }

    protected override int ParseInputPart1(string input) =>
        input.ToInt();
}