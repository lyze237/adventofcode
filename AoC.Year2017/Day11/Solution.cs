﻿namespace AoC.Year2017.Day11;

public class Solution : Day<string[]>
{
    protected override object DoPart1(string[] input)
    {
        var origin = new Point(0, 0);
        var coords = new Point(0, 0);

        coords = input.Aggregate(coords, (current, input) => Move(input, current));

        return GetDistance(origin, coords);
    }

    protected override object DoPart2(string[] input)
    {
        var origin = new Point(0, 0);
        var coords = new Point(0, 0);
        var maxDistance = 0L;
            
        foreach (var line in input)
        {
            coords = Move(line, coords);
            maxDistance = Math.Max(maxDistance, GetDistance(origin, coords));
        }

        return maxDistance;
    }
    
    private static Point Move(string d, Point position)
    {
        return d switch
        {
            "n" => position with { Y = position.Y + 2 },
            "s" => position with { Y = position.Y - 2 },
            "ne" => new Point(position.X + 1, position.Y + 1),
            "nw" => new Point(position.X - 1, position.Y + 1),
            "se" => new Point(position.X + 1, position.Y - 1),
            "sw" => new Point(position.X - 1, position.Y - 1),
            _ => throw new ArgumentException(d)
        };
    }
        
    private static long GetDistance(Point origin, Point position)
    {
        var xM = Math.Abs(position.X - origin.X);
        var yM = (Math.Abs(position.Y - origin.Y) - xM) / 2;

        return xM + yM;
    }
    
    protected override string[] ParseInputPart1(string input) => 
        input.Split(",");
}