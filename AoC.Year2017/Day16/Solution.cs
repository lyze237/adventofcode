﻿namespace AoC.Year2017.Day16;

public class Solution : Day<string[]>
{
    protected override object DoPart1(string[] input)
    {
        var programs = "abcdefghijklmnop";

        foreach (var task in input)
        {
            var parameter = task[1..];
            switch (task[0])
            {
                case 's':
                    var amount = Convert.ToInt32(parameter);

                    var temp = programs[^amount..];
                    programs = programs.Remove(programs.Length - amount);

                    programs = temp + programs;
                    break;
                case 'x':
                    var strings = parameter.Split('/');

                    var indexA = Convert.ToInt32(strings[0]);
                    var indexB = Convert.ToInt32(strings[1]);

                    var programsArray = programs.ToCharArray();
                    programsArray[indexA] = programs[indexB];
                    programsArray[indexB] = programs[indexA];

                    programs = string.Join(string.Empty, programsArray);
                    break;
                case 'p':
                    strings = parameter.Split('/');

                    var programA = strings[0];
                    var programB = strings[1];

                    indexA = programs.IndexOf(programA, StringComparison.Ordinal);
                    indexB = programs.IndexOf(programB, StringComparison.Ordinal);

                    programsArray = programs.ToCharArray();
                    programsArray[indexA] = programs[indexB];
                    programsArray[indexB] = programs[indexA];

                    programs = string.Join(string.Empty, programsArray);
                    break;
            }
        }

        return programs;
    }

    protected override object DoPart2(string[] input)
    {
        const string initialPrograms = "abcdefghijklmnop";
        var programs = "abcdefghijklmnop";

        for (var i = 0; i < 1_000_000_000 % 60; i++)
        {
            foreach (var task in input)
            {
                var parameter = task[1..];
                switch (task[0])
                {
                    case 's':
                        var amount = Convert.ToInt32(parameter);

                        var temp = programs[^amount..];
                        programs = programs.Remove(programs.Length - amount);

                        programs = temp + programs;
                        break;
                    case 'x':
                        var strings = parameter.Split('/');

                        var indexA = Convert.ToInt32(strings[0]);
                        var indexB = Convert.ToInt32(strings[1]);

                        var programsArray = programs.ToCharArray();
                        programsArray[indexA] = programs[indexB];
                        programsArray[indexB] = programs[indexA];

                        programs = string.Join(string.Empty, programsArray);
                        break;
                    case 'p':
                        strings = parameter.Split('/');

                        var programA = strings[0];
                        var programB = strings[1];

                        indexA = programs.IndexOf(programA, StringComparison.Ordinal);
                        indexB = programs.IndexOf(programB, StringComparison.Ordinal);

                        programsArray = programs.ToCharArray();
                        programsArray[indexA] = programs[indexB];
                        programsArray[indexB] = programs[indexA];

                        programs = string.Join(string.Empty, programsArray);
                        break;
                }
            }
        }

        return programs;
    }
    
    protected override string[] ParseInputPart1(string input) => 
        input.Split(",");
}