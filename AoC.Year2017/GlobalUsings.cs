// Global using directives

global using System.Text.RegularExpressions;
global using AoC.Extensions.Comparers;
global using AoC.Extensions.Data;
global using AoC.Extensions.Extensions;
global using AoC.Framework;
global using AoC.Framework.Logging;