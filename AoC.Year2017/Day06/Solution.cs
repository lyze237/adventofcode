﻿namespace AoC.Year2017.Day06;

public class Solution : Day<int[]>
{
    protected override object DoPart1(int[] input)
    {
        var configs = new List<int[]>();

        while (!configs.Any(x => x.SequenceEqual(input)))
        {
            configs.Add(input.ToArray());
            RedistributeBlocks(input);
        }

        return configs.Count;
    }
    
    protected override object DoPart2(int[] input)
    {
        var configs = new List<int[]>();

        while (!configs.Any(x => x.SequenceEqual(input)))
        {
            configs.Add((int[])input.Clone());
            RedistributeBlocks(input);
        }

        var seenIndex = configs.IndexOf(configs.First(x => x.SequenceEqual(input)));
        var steps = configs.Count - seenIndex;

        return steps;
    }
    
    private static void RedistributeBlocks(int[] banks)
    {
        var idx = banks.ToList().IndexOf(banks.Max());
        var blocks = banks[idx];

        banks[idx++] = 0;

        while (blocks > 0)
        {
            if (idx >= banks.Length)
            {
                idx = 0;
            }

            banks[idx++]++;
            blocks--;
        }
    }



    protected override int[] ParseInputPart1(string input) => 
        input.Split("\t").Select(int.Parse).ToArray();
}