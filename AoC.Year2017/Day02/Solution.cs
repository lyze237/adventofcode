﻿namespace AoC.Year2017.Day02;

public class Solution : Day<string[]>
{
    protected override object DoPart1(string[] input)
    {
        var sum = 0;
        foreach (var line in input)
        {
            var inputs = line.Split('\t');
            var min = int.MaxValue;
            var max = int.MinValue;
            
            foreach (var i in inputs)
            {
                var number = Convert.ToInt32(i);

                if (number < min)
                    min = number;

                if (number > max)
                    max = number;
            }
            sum += max - min;
        }

        return sum;
    }

    protected override object DoPart2(string[] input)
    {
        var sum = 0;
        foreach (var line in input)
        {
            var inputs = line.Split('\t');
            foreach (var inputLeft in inputs)
            {
                var numberLeft = Convert.ToInt32(inputLeft);
                    
                foreach (var inputRight in inputs)
                {
                    var numberRight = Convert.ToInt32(inputRight);

                    var max = Math.Max(numberLeft, numberRight);
                    var min = Math.Min(numberLeft, numberRight);

                    if (max % min == 0 && max != min)
                        sum += max / min;
                }
            }
        }

        return sum / 2;
    }

    protected override string[] ParseInputPart1(string input) => 
        input.Split("\n");
}