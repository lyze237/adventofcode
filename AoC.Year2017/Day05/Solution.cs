﻿namespace AoC.Year2017.Day05;

public class Solution : Day<int[]>
{
    protected override object DoPart1(int[] input)
    {
        var pointerIndex = 0;
        var jumps = 0;

        for (; pointerIndex >= 0 && pointerIndex < input.Length; jumps++)
        {
            var instruction = input[pointerIndex];

            input[pointerIndex]++;

            pointerIndex += instruction;
        }

        return jumps;
    }

    protected override object DoPart2(int[] input)
    {
        var pointerIndex = 0;
        var jumps = 0;

        for (; pointerIndex >= 0 && pointerIndex < input.Length; jumps++)
        {
            var instruction = input[pointerIndex];

            if (instruction >= 3)
                input[pointerIndex]--;
            else
                input[pointerIndex]++;

            pointerIndex += instruction;
        }

        return jumps;
    }

    protected override int[] ParseInputPart1(string input) =>
        input.Split("\n").Select(num => num.ToInt()).ToArray();
}