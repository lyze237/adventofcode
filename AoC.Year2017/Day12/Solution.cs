﻿namespace AoC.Year2017.Day12;

public partial class Solution : Day<string[]>
{
    protected override object DoPart1(string[] input)
    {
        var nodes = input.Select(line => MyRegex().Match(line))
            .Select(match => new Node(Convert.ToInt32(match.Groups[1].Value))).ToList();

        foreach (var line in input)
        {
            var match = MyRegex().Match(line);
            var neighbours = match.Groups[2].Value
                .Split(',')
                .Select(s => nodes
                    .First(node => node.Id == Convert.ToInt32(s.Trim()))
                ).ToArray();

            nodes.First(node => node.Id == Convert.ToInt32(match.Groups[1].Value)).Path
                .AddRange(neighbours);
        }

        return nodes.First(node => node.Id == 0)
            .FindAllNeighbours().Count;
    }

    protected override object DoPart2(string[] input)
    {
        var nodes = input.Select(line => MyRegex().Match(line))
            .Select(match => new Node(Convert.ToInt32(match.Groups[1].Value))).ToList();

        foreach (var line in input)
        {
            var match = MyRegex().Match(line);
            var neighbours = match.Groups[2].Value
                .Split(',')
                .Select(s => nodes
                    .First(node => node.Id == Convert.ToInt32(s.Trim()))
                ).ToArray();

            nodes.First(node => node.Id == Convert.ToInt32(match.Groups[1].Value)).Path
                .AddRange(neighbours);
        }

        var cnt = 0;
        while (nodes.Count > 0)
        {
            var allNeighbours = nodes[0].FindAllNeighbours();
            nodes.RemoveAll(node => allNeighbours.Contains(node));
            cnt++;
        }

        return cnt;
    }

    public class Node(int id)
    {
        public int Id { get; } = id;
        public List<Node> Path { get; } = [];

        private bool Equals(Node other)
        {
            return Id == other.Id;
        }

        public override bool Equals(object? obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj.GetType() == GetType() && Equals((Node)obj);
        }

        public override int GetHashCode() => 
            Id;

        public override string ToString() => 
            $"{nameof(Id)}: {Id}, {nameof(Path)}.Count: {Path.Count}";

        public List<Node> FindAllNeighbours()
        {
            var neighbours = new List<Node>();
            FindAllNeighbours(neighbours);
            return neighbours;
        }

        private void FindAllNeighbours(List<Node> alreadyVisited)
        {
            alreadyVisited.Add(this);
            foreach (var node in Path.Where(node => !alreadyVisited.Contains(node)))
            {
                node.FindAllNeighbours(alreadyVisited);
            }
        }
    }
    
    protected override string[] ParseInputPart1(string input) => 
        input.Split("\n");
    
    [GeneratedRegex(@"(\d*) <-> ([\d, ]*)")]
    private static partial Regex MyRegex();
}