﻿namespace AoC.Year2017.Day09;

public class Solution : Day
{
    public override object DoPart1(string input)
    {
        var level = 0;
        var sumLevel = 0;
        var inGarbage = false;

        for (var i = 0; i < input.Length; i++)
        {
            var c = input[i];

            switch (c)
            {
                case '!':
                    i++;
                    break;

                case '{':
                    if (!inGarbage)
                    {
                        level++;
                    }

                    break;
                case '}':
                    if (!inGarbage)
                    {
                        sumLevel += level--;
                    }

                    break;

                case '<':
                    inGarbage = true;
                    break;
                case '>':
                    inGarbage = false;
                    break;
            }
        }

        return sumLevel;
    }

    public override object DoPart2(string input)
    {
        var garbageCount = 0;
        var inGarbage = false;
            
        for (var i = 0; i < input.Length; i++)
        {
            var c = input[i];

            switch (c)
            {
                case '!':
                    i++;
                    break;
                        
                case '{':
                    break;
                case '}':
                    break;
                        
                case '<':
                    if (!inGarbage)
                        garbageCount--;
                    inGarbage = true;
                    break;
                case '>':
                    inGarbage = false;
                    break;
            }
                
            if (inGarbage && c != '!')
            {
                garbageCount++;
            }
        }
            
        return garbageCount;
    }
}