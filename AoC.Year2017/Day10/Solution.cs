﻿using System.Text;

namespace AoC.Year2017.Day10;

public class Solution : Day
{
    public override object DoPart1(string input)
    {
        var lengths = input.Split(",").Select(s => Convert.ToInt32(s)).ToArray();
        var tmpList = new int[256];
        for (var i = 0; i < tmpList.Length; i++)
            tmpList[i] = i;

        var list = new WrapArray<int>(tmpList);
        var cursor = 0;

        for (var i = 0; i < lengths.Length; i++)
        {
            for (var startIndex = cursor; startIndex < cursor + lengths[i] / 2; startIndex++)
            {
                (list[cursor + lengths[i] - 1 - (startIndex - cursor)], list[startIndex]) = (list[startIndex],
                    list[cursor + lengths[i] - 1 - (startIndex - cursor)]);
            }

            cursor += lengths[i] + i;
        }

        return tmpList[0] * tmpList[1];
    }

    public override object DoPart2(string input)
    {
        var lengths = input.ToCharArray().Select(c => (int)c)
            .Append(17).Append(31).Append(73).Append(47).Append(23)
            .ToArray();
        var tmpList = new int[256];
        for (var i = 0; i < tmpList.Length; i++)
            tmpList[i] = i;

        var list = new WrapArray<int>(tmpList);

        var cursor = 0;
        var skipSize = 0;

        for (var z = 0; z < 64; z++)
        {
            for (var i = 0; i < lengths.Length; i++, skipSize++)
            {
                for (var startIndex = cursor; startIndex < cursor + lengths[i] / 2; startIndex++)
                {
                    (list[cursor + lengths[i] - 1 - (startIndex - cursor)], list[startIndex]) = (list[startIndex],
                        list[cursor + lengths[i] - 1 - (startIndex - cursor)]);
                }

                cursor += lengths[i] + skipSize;
            }
        }

        var xorList = new List<int>();
        for (var i = 0; i < list.Length; i += 16)
        {
            var toXor = list.Array.Skip(i).Take(16).ToArray();
            var xorResult = toXor[0];

            for (var j = 1; j < toXor.Length; j++)
                xorResult ^= toXor[j];

            xorList.Add(xorResult);
        }

        var ret = new StringBuilder();
        foreach (var element in xorList)
            ret.Append(element.ToString("X").PadLeft(2, '0'));

        return ret.ToString().ToLower();
    }

    private class WrapArray<T>(T[] array)
    {
        public T[] Array => array;

        public T this[int index]
        {
            get => array[index % array.Length];
            set => array[index % array.Length] = value;
        }

        public int Length => array.Length;
    }
}