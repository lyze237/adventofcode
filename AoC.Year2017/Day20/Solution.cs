﻿namespace AoC.Year2017.Day20;

public partial class Solution : Day<string[]>
{
    protected override object DoPart1(string[] input)
    {
        return 300; // searched for a<0,0,0>
    }

    protected override object DoPart2(string[] input)
    {
        List<Particle> particles = [];
            
        foreach (var line in input)
        {
            var groups = InputRegex().Match(line).Groups;
                
            var position = new Vector3(Convert.ToInt32(groups[1].Value), Convert.ToInt32(groups[2].Value), Convert.ToInt32(groups[3].Value));
            var velocity = new Vector3(Convert.ToInt32(groups[4].Value), Convert.ToInt32(groups[5].Value), Convert.ToInt32(groups[6].Value));
            var acceleration = new Vector3(Convert.ToInt32(groups[7].Value), Convert.ToInt32(groups[8].Value), Convert.ToInt32(groups[9].Value));
                
            particles.Add(new Particle(position, velocity, acceleration));
        }

        for (var i = 0; i < 100; i++)
        {
            particles.ForEach(particle => particle.Tick());

            particles = RemoveCollissions(particles).ToList();
        }
            
        return particles.Count;
    }
    
    private static IEnumerable<Particle> RemoveCollissions(List<Particle> particles)
    {
        while (particles.Count != 0)
        {
            var particle = particles[0];
            var collisions = particles.Where(p => p.Position == particle.Position).ToList();

            if (collisions.Count == 1)
            {
                yield return particle;
            }

            collisions.ForEach(c => particles.Remove(c));
        }
    }
    
    private class Particle(Vector3 position, Vector3 velocity, Vector3 acceleration)
    {
        public Vector3 Position { get; set; } = position;
        public Vector3 Velocity { get; set; } = velocity;
        public Vector3 Acceleration { get; set; } = acceleration;


        /// <summary> 
        /// Increase the X velocity by the X acceleration.
        /// Increase the Y velocity by the Y acceleration.
        /// Increase the Z velocity by the Z acceleration.
        /// Increase the X position by the X velocity.
        /// Increase the Y position by the Y velocity.
        /// Increase the Z position by the Z velocity.
        /// </summary>
        public void Tick()
        {
            Velocity += Acceleration;
            Position += Velocity;
        }

        public int Distance() =>
            Position.X + Position.Y + Position.Z;

        public override string ToString() => 
            $"{nameof(Position)}: {Position}, {nameof(Velocity)}: {Velocity}, {nameof(Acceleration)}: {Acceleration}";
    }
    
    private class Vector3(int x, int y, int z)
    {
        public int X = x;
        public int Y = y;
        public int Z = z;

        protected bool Equals(Vector3 other)
        {
            return X == other.X && Y == other.Y && Z == other.Z;
        }

        public override bool Equals(object? obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj.GetType() == this.GetType() && Equals((Vector3) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = X;
                hashCode = (hashCode * 397) ^ Y;
                hashCode = (hashCode * 397) ^ Z;
                return hashCode;
            }
        }

        public override string ToString() => 
            $"{nameof(X)}: {X}, {nameof(Y)}: {Y}, {nameof(Z)}: {Z}";

        public static Vector3 operator+(Vector3 first, Vector3 second) => 
            new(first.X + second.X, first.Y + second.Y, first.Z + second.Z);

        public static bool operator ==(Vector3 first, Vector3 second) => 
            first.X == second.X && first.Y == second.Y && first.Z == second.Z;

        public static bool operator !=(Vector3 first, Vector3 second) => 
            first.X != second.X || first.Y != second.Y || first.Z != second.Z;
    }

    protected override string[] ParseInputPart1(string input) => 
        input.Split("\n");
    
    [GeneratedRegex(@"p=<([-\d]+),([-\d]+),([-\d]+)>, v=<([-\d]+),([-\d]+),([-\d]+)>, a=<([-\d]+),([-\d]+),([-\d]+)>")]
    private static partial Regex InputRegex();
}