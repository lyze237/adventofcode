﻿namespace AoC.Year2017.Day08;

public partial class Solution : Day<string[]>
{
    protected override object DoPart1(string[] input)
    {
        var instructions = new List<Instruction>();
        var registers = new Dictionary<string, int>();
        foreach (var line in input)
        {
            var match = InstructionRegex().Match(line);
            instructions.Add(new Instruction(match.Groups[1].Value, match.Groups[4].Value,
                int.Parse(match.Groups[3].Value), match.Groups[2].Value.Equals("inc"),
                match.Groups[5].Value, int.Parse(match.Groups[6].Value)));
            registers[match.Groups[1].Value] = 0;
        }

        foreach (var instruction in instructions)
        {
            switch (instruction.Condition)
            {
                case ">":
                    if (registers[instruction.OnWho] > instruction.Amount)
                        registers[instruction.Name] =
                            instruction.IsInc
                                ? registers[instruction.Name] + instruction.ChangeAmount
                                : registers[instruction.Name] - instruction.ChangeAmount;
                    break;
                case "<":
                    if (registers[instruction.OnWho] < instruction.Amount)
                        registers[instruction.Name] =
                            instruction.IsInc
                                ? registers[instruction.Name] + instruction.ChangeAmount
                                : registers[instruction.Name] - instruction.ChangeAmount;
                    break;
                case "<=":
                    if (registers[instruction.OnWho] <= instruction.Amount)
                        registers[instruction.Name] =
                            instruction.IsInc
                                ? registers[instruction.Name] + instruction.ChangeAmount
                                : registers[instruction.Name] - instruction.ChangeAmount;
                    break;
                case ">=":
                    if (registers[instruction.OnWho] >= instruction.Amount)
                        registers[instruction.Name] =
                            instruction.IsInc
                                ? registers[instruction.Name] + instruction.ChangeAmount
                                : registers[instruction.Name] - instruction.ChangeAmount;
                    break;
                case "==":
                    if (registers[instruction.OnWho] == instruction.Amount)
                        registers[instruction.Name] =
                            instruction.IsInc
                                ? registers[instruction.Name] + instruction.ChangeAmount
                                : registers[instruction.Name] - instruction.ChangeAmount;
                    break;
                case "!=":
                    if (registers[instruction.OnWho] != instruction.Amount)
                        registers[instruction.Name] =
                            instruction.IsInc
                                ? registers[instruction.Name] + instruction.ChangeAmount
                                : registers[instruction.Name] - instruction.ChangeAmount;
                    break;
            }
        }

        return registers.Max(pair => pair.Value);
    }

    protected override object DoPart2(string[] input)
    {
        var instructions = new List<Instruction>();
        var registers = new Dictionary<string, int>();
        var maxValue = int.MinValue;
        foreach (var line in input)
        {
            var match = InstructionRegex().Match(line);
            instructions.Add(new Instruction(match.Groups[1].Value, match.Groups[4].Value,
                int.Parse(match.Groups[3].Value), match.Groups[2].Value.Equals("inc"),
                match.Groups[5].Value, int.Parse(match.Groups[6].Value)));
            registers[match.Groups[1].Value] = 0;
        }

        foreach (var instruction in instructions)
        {
            switch (instruction.Condition)
            {
                case ">":
                    if (registers[instruction.OnWho] > instruction.Amount)
                        registers[instruction.Name] =
                            instruction.IsInc
                                ? registers[instruction.Name] + instruction.ChangeAmount
                                : registers[instruction.Name] - instruction.ChangeAmount;
                    break;
                case "<":
                    if (registers[instruction.OnWho] < instruction.Amount)
                        registers[instruction.Name] =
                            instruction.IsInc
                                ? registers[instruction.Name] + instruction.ChangeAmount
                                : registers[instruction.Name] - instruction.ChangeAmount;
                    break;
                case "<=":
                    if (registers[instruction.OnWho] <= instruction.Amount)
                        registers[instruction.Name] =
                            instruction.IsInc
                                ? registers[instruction.Name] + instruction.ChangeAmount
                                : registers[instruction.Name] - instruction.ChangeAmount;
                    break;
                case ">=":
                    if (registers[instruction.OnWho] >= instruction.Amount)
                        registers[instruction.Name] =
                            instruction.IsInc
                                ? registers[instruction.Name] + instruction.ChangeAmount
                                : registers[instruction.Name] - instruction.ChangeAmount;
                    break;
                case "==":
                    if (registers[instruction.OnWho] == instruction.Amount)
                        registers[instruction.Name] =
                            instruction.IsInc
                                ? registers[instruction.Name] + instruction.ChangeAmount
                                : registers[instruction.Name] - instruction.ChangeAmount;
                    break;
                case "!=":
                    if (registers[instruction.OnWho] != instruction.Amount)
                        registers[instruction.Name] =
                            instruction.IsInc
                                ? registers[instruction.Name] + instruction.ChangeAmount
                                : registers[instruction.Name] - instruction.ChangeAmount;
                    break;
            }

            if (maxValue < registers[instruction.Name])
                maxValue = registers[instruction.Name];
        }

        return maxValue;
    }

    public class Instruction(string name, string onWho, int changeAmount, bool isInc, string condition, int amount)
    {
        public string Name { get; set; } = name;

        public string OnWho { get; set; } = onWho;
        public int ChangeAmount { get; set; } = changeAmount;
        public bool IsInc { get; set; } = isInc;
        public string Condition { get; set; } = condition;
        public int Amount { get; set; } = amount;
    }

    protected override string[] ParseInputPart1(string input) => 
        input.Split("\n");
    
    [GeneratedRegex(@"(\w+) (dec|inc) (-?\d+) if (\w+) ([<=>!]{1,2}) (-?\d+)")]
    private static partial Regex InstructionRegex();
}