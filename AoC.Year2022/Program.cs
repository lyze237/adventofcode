﻿using System.Reflection;

await new AocBuilder()
    .SetYear(2022)
    .SetDays(Enumerable.Range(1, 15).ToArray())
    .SetLogLevel(Logger.LogLevel.Solve)
    .SkipExamples()
    .Execute(Assembly.GetAssembly(typeof(Program))!);
