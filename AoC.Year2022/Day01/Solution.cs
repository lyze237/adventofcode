﻿namespace AoC.Year2022.Day01;

public class Solution : Day
{
    public override object DoPart1(string input) => 
        SortElves(input).First();

    public override object DoPart2(string input) => 
        SortElves(input).Take(3).Sum();

    private static IEnumerable<long> SortElves(string input) =>
        input.Split("\n\n")
            .Select(g => g.Split("\n"))
            .Select(g => g.Sum(Convert.ToInt64))
            .OrderByDescending(g => g);
}