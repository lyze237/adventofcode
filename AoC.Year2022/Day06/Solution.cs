﻿namespace AoC.Year2022.Day06;

public class Solution : Day
{
    public override object DoPart1(string input) =>
        FindPacket(4, input);

    public override object DoPart2(string input) => 
        FindPacket(14, input);
    
    private static int FindPacket(int length, string input)
    {
        for (var i = length; i < input.Length; i++)
            if (input.Substring(i - length, length).Distinct().Count() == length)
                return i;

        return -1;
    }
}