﻿using System.Text;

namespace AoC.Year2018.Day07;

public class Solution : Day<string[]>
{
    protected override object DoPart1(string[] input)
    {
        var dependencies = new List<(char pre, char post)>();
        input.ToList().ForEach(x => dependencies.Add((x[5], x[36])));

        var allSteps = dependencies
            .Select(x => x.pre)
            .Concat(dependencies.Select(x => x.post))
            .Distinct()
            .OrderBy(x => x).ToList();

        var result = new StringBuilder();
        while (allSteps.Count > 0)
        {
            var valid = allSteps.First(s => dependencies.All(d => d.post != s));

            result.Append(valid);

            allSteps.Remove(valid);
            dependencies.RemoveAll(d => d.pre == valid);
        }

        return result.ToString();
    }

    protected override object DoPart2(string[] input)
    {
        var deps = new List<(char Pre, char Post)>();
        input.ToList().ForEach(x => deps.Add((x[5], x[36])));

        var allSteps = deps
            .Select(d => d.Pre)
            .Concat(deps.Select(d => d.Post))
            .Distinct()
            .OrderBy(d => d)
            .ToList();

        var workers = new List<int>(5) { 0, 0, 0, 0, 0 };
        var currentSecond = 0;
        var doneList = new List<(char step, int finish)>();

        while (allSteps.Count > 0 || workers.Count(w => w > currentSecond) > 0)
        {
            foreach (var x in doneList.Where(d => d.finish <= currentSecond).ToList())
            {
                deps.RemoveAll(d => d.Pre == x.step);
                doneList.Remove(x);
            }

            var valid = allSteps.Where(s => deps.All(d => d.Post != s)).ToList();

            for (var w = 0; w < workers.Count(x => x <= currentSecond) && valid.Count > 0; w++)
            {
                workers[w] = valid[0] - 'A' + 61 + currentSecond;
                allSteps.Remove(valid[0]);
                doneList.Add((valid[0], workers[w]));
                valid.RemoveAt(0);
            }

            currentSecond++;
        }

        return currentSecond;
    }

    protected override string[] ParseInputPart1(string input) =>
        input.Split("\n");
}