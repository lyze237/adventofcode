﻿namespace AoC.Year2018.Day14;

public class Solution : Day<string[]>
{
    protected override object DoPart1(string[] input)
    {
        var recipes = input[0].Select(c => c - '0').ToList();

        var elves = new List<int> { 0, 1 };

        var n = Convert.ToInt32(input[1]);

        while (recipes.Count < n + 10)
        {
            var newRecipe = elves.Sum(e => recipes[e]);

            recipes.AddRange(("" + newRecipe).Select(c => c - '0').ToList());

            for (var i = 0; i < elves.Count; i++)
                elves[i] = (elves[i] + recipes[elves[i]] + 1) % recipes.Count;
        }

        return string.Join("", recipes.Skip(n).Take(10));
    }

    protected override object DoPart2(string[] input)
    {
        var recipes = input[0].Select(c => c - '0').ToList();
        var recipeToFind = input[1].Select(c => c - '0').ToList();

        var elves = new List<int> { 0, 1 };

        while (true)
        {
            var newRecipe = elves.Sum(e => recipes[e]);

            foreach (var numberToAdd in newRecipe.ToString().Select(c => c - '0'))
            {
                recipes.Add(numberToAdd);
                if (recipes.Count >= recipeToFind.Count && recipes.Skip(recipes.Count - recipeToFind.Count).SequenceEqual(recipeToFind))
                    return recipes.Count - recipeToFind.Count;
            }

            for (var i = 0; i < elves.Count; i++)
                elves[i] = (elves[i] + recipes[elves[i]] + 1) % recipes.Count;
        }
    }

    protected override string[] ParseInputPart1(string input) =>
        input.Split("\n");
}