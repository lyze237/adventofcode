﻿namespace AoC.Year2018.Day05;

public class Solution : Day
{
    public override object DoPart1(string input)
    {
        var stack = new Stack<char>();

        foreach (var c in input)
        {
            if (stack.Count == 0)
            {
                stack.Push(c);
                continue;
            }

            var inStack = stack.Peek();
            if (c != inStack && char.ToUpper(c) == char.ToUpper(inStack))
                stack.Pop();
            else
                stack.Push(c);
        }

        return stack.Count;
    }

    public override object DoPart2(string input)
    {
        var result = new Dictionary<char, int>();
        for (int i = 'a'; i <= 'z'; i++)
        {
            var stack = new Stack<char>();
                    
            var currentInput = new string(input);
            currentInput = currentInput.Replace(((char) i).ToString(), "").Replace(char.ToUpper((char) i).ToString(), "");
                
            foreach (var c in currentInput)
            {
                if (stack.Count == 0)
                {
                    stack.Push(c);
                    continue;
                }

                var inStack = stack.Peek();
                if (c != inStack && char.ToUpper(c) == char.ToUpper(inStack))
                    stack.Pop();
                else
                    stack.Push(c);
            }
                
            result.Add((char) i, stack.Count);
        }
            
        return result.MinBy(r => r.Value).Value;
    }
}