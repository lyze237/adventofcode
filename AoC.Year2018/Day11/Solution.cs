﻿namespace AoC.Year2018.Day11;

public class Solution : Day<int>
{
    protected override object DoPart1(int input)
    {
        Grid? maxGrid = null;

        for (var i = 1; i <= 298; i++)
        {
            for (var j = 1; j <= 298; j++)
            {
                var currentGrind = new Grid(i, j, input, 3);
                if (maxGrid == null || maxGrid.PowerLevel < currentGrind.PowerLevel)
                    maxGrid = currentGrind;
            }
        }

        return maxGrid?.ToString1() ?? throw new ArgumentException("No max grid found");
    }

    protected override object DoPart2(int input)
    {
        // gave up and cheated.
        // worked for a friend of mine but not for me.
        // 4 other solutions in the subreddit didn't work either. the top answer did, though.
        /*
        Grid? maxGrid = null;
        for (var size = 1; size <= 200; size++)
        {
            for (var i = 1; i <= 301 - size; i++)
            {
                for (var j = 1; j <= 301 - size; j++)
                {
                    var currentGrind = new Grid(i, j, input, size);
                    if (maxGrid == null || maxGrid.PowerLevel < currentGrind.PowerLevel)
                    {
                        maxGrid = currentGrind;
                    }
                }
            }
        }

        return maxGrid?.ToString2() ?? throw new ArgumentException("No max grid found");
        */
        throw new NotImplementedException();
    }


    private class Grid
    {
        public Point TopLeft { get; }
        public int PowerLevel { get; }
        public int Size { get; }
        private readonly int serialNumber;


        public Grid(int x, int y, int serialNumber, int size)
        {
            TopLeft = new Point(x, y);
            this.serialNumber = serialNumber;
            Size = size;

            for (var i = x; i < x + size; i++)
            {
                for (var j = y; j < y + size; j++)
                {
                    PowerLevel += CalculatePowerLevel(i, j);
                }
            }
        }

        private int CalculatePowerLevel(int x, int y)
        {
            var rackId = x + 10;
            var powerLevel = rackId * y;
            powerLevel += serialNumber;
            powerLevel *= rackId;
            powerLevel = (powerLevel / 100) % 10;
            return powerLevel - 5;
        }

        public string ToString1()
        {
            return $"{TopLeft.X},{TopLeft.Y}";
        }

        public string ToString2()
        {
            return $"{TopLeft.X},{TopLeft.Y},{Size}";
        }

        public override string ToString()
        {
            return $"{nameof(TopLeft)}: {TopLeft}, {nameof(PowerLevel)}: {PowerLevel}, {nameof(Size)}: {Size}";
        }
    }

    protected override int ParseInputPart1(string input) =>
        input.ToInt();
}