﻿namespace AoC.Year2018.Day04;

public class Solution : Day<string[]>
{
    protected override object DoPart1(string[] input)
    {
        var times = new Dictionary<int, Dictionary<int, int>>();

        var guard = 0;
        var at = 0;

        foreach (var line in input)
        {
            if (line.Contains("falls asleep"))
            {
                at = Convert.ToInt32(line.Split(":")[1].Split("]")[0]);
            }
            else if (line.Contains("wakes up"))
            {
                var wakeup = Convert.ToInt32(line.Split(":")[1].Split("]")[0]);
                if (!times.ContainsKey(guard))
                    times.Add(guard, new Dictionary<int, int>());

                for (var min = at; min < wakeup; min++)
                {
                    if (!times[guard].ContainsKey(min))
                        times[guard].Add(min, 0);

                    times[guard][min]++;
                }
            }
            else
            {
                guard = Convert.ToInt32(line.Split("#")[1].Split(" ")[0]);
            }
        }


        var sleepy = times
            .ToDictionary(t => t.Key, t => t.Value.Sum(tv => tv.Value)).MaxBy(t => t.Value).Key;

        var sleepyAt = times[sleepy].MaxBy(t => t.Value).Key;

        return sleepy * sleepyAt;
    }

    protected override object DoPart2(string[] input)
    {
        var times = new Dictionary<int, Dictionary<int, int>>();

        var guard = 0;
        var at = 0;

        foreach (var line in input)
        {
            if (line.Contains("falls asleep"))
            {
                at = Convert.ToInt32(line.Split(":")[1].Split("]")[0]);
            }
            else if (line.Contains("wakes up"))
            {
                var wakeup = Convert.ToInt32(line.Split(":")[1].Split("]")[0]);
                if (!times.ContainsKey(guard))
                    times.Add(guard, new Dictionary<int, int>());

                for (var min = at; min < wakeup; min++)
                {
                    if (!times[guard].ContainsKey(min))
                        times[guard].Add(min, 0);

                    times[guard][min]++;
                }
            }
            else
            {
                guard = Convert.ToInt32(line.Split("#")[1].Split(" ")[0]);
            }
        }

        var sleepy = times
            .ToDictionary(t => t.Key, t => t.Value.MaxBy(tv => tv.Value)).MaxBy(t => t.Value.Value).Key;

        var sleepyAt = times[sleepy].MaxBy(t => t.Value).Key;

        return sleepy * sleepyAt;
    }

    protected override string[] ParseInputPart1(string input) =>
        input.Split("\n").OrderBy(l => l).ToArray();
}