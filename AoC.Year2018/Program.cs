﻿using System.Reflection;

var days = Enumerable.Range(1, 14).ToList();
days.Remove(13); // Where did you go?

await new AocBuilder()
    .SetYear(2018)
    .SetDays(days.ToArray())
    .SetLogLevel(Logger.LogLevel.Solve)
    .SkipExamples()
    .Execute(Assembly.GetAssembly(typeof(Program))!);