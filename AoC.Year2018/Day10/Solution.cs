﻿using System.Text;

namespace AoC.Year2018.Day10;

public partial class Solution : Day<Solution.Star[]>
{
    protected override object DoPart1(Star[] input)
    {
        var topLeft = new Point(input.Min(s => s.Position.X), input.Min(s => s.Position.Y));
        var bottomRight = new Point(input.Max(s => s.Position.X), input.Max(s => s.Position.Y));

        while (true)
        {
            var oldStars = input.ToList();

            input.ForEach(s => s.Update());

            var newTopLeft = new Point(input.Min(s => s.Position.X), input.Min(s => s.Position.Y));
            var newBottomRight = new Point(input.Max(s => s.Position.X), input.Max(s => s.Position.Y));

            if (Math.Abs(newBottomRight.X - newTopLeft.X) > Math.Abs(bottomRight.X - topLeft.X) || Math.Abs(newBottomRight.Y - newTopLeft.Y) > Math.Abs(bottomRight.Y - topLeft.Y))
            {
                input.ForEach(s => s.Update(false));
                // print
                var toPrint = new StringBuilder();

                for (var y = topLeft.Y; y <= bottomRight.Y; y++)
                {
                    for (var x = topLeft.X; x <= bottomRight.X; x++)
                        toPrint.Append(oldStars.Any(s => s.Position.X == x && s.Position.Y == y) ? '#' : ' ');

                    toPrint.Append('\n');
                }

                return toPrint.ToString().Ocr();
            }

            topLeft = newTopLeft;
            bottomRight = newBottomRight;
        }
    }

    protected override object DoPart2(Star[] input)
    {
        var topLeft = new Point(input.Min(s => s.Position.X), input.Min(s => s.Position.Y));
        var bottomRight = new Point(input.Max(s => s.Position.X), input.Max(s => s.Position.Y));

        var seconds = 0;

        while (true)
        {
            input.ForEach(s => s.Update());

            var newTopLeft = new Point(input.Min(s => s.Position.X), input.Min(s => s.Position.Y));
            var newBottomRight = new Point(input.Max(s => s.Position.X), input.Max(s => s.Position.Y));

            if (Math.Abs(newBottomRight.X - newTopLeft.X) > Math.Abs(bottomRight.X - topLeft.X) ||
                Math.Abs(newBottomRight.Y - newTopLeft.Y) > Math.Abs(bottomRight.Y - topLeft.Y))
            {
                input.ForEach(s => s.Update(false));

                return seconds;
            }

            topLeft = newTopLeft;
            bottomRight = newBottomRight;

            seconds++;
        }
    }

    public partial class Star
    {
        public Point Position { get; set; }
        public Point Velocity { get; set; }

        public Star(string line)
        {
            var match = StarRegex().Match(line);

            Position = new Point(match.Groups["posX"].Value.ToInt(), match.Groups["posY"].Value.ToInt());
            Velocity = new Point(match.Groups["velX"].Value.ToInt(), match.Groups["velY"].Value.ToInt());
        }

        public void Update(bool forward = true) => Position = forward
            ? new Point(Position.X + Velocity.X, Position.Y + Velocity.Y)
            : new Point(Position.X - Velocity.X, Position.Y - Velocity.Y);

        public override string ToString() =>
            $"{nameof(Position)}: {Position}, {nameof(Velocity)}: {Velocity}";

        [GeneratedRegex(@"position=<(?<posX>[- \d]+), (?<posY>[- \d]+)> velocity=<(?<velX>[- \d]+), (?<velY>[- \d]+)>")]
        private static partial Regex StarRegex();
    }

    protected override Star[] ParseInputPart1(string input) =>
        input.Split("\n").Select(line => new Star(line)).ToArray();
}