﻿namespace AoC.Year2018.Day09;

public class Solution : Day<string[]>
{
    protected override object DoPart1(string[] input)
    {
        var players = input[0].ToInt();
        var maxPoints = input[6].ToInt();

        var scores = new Dictionary<int, int>();
        var circle = new LinkedList<int>();
        circle.AddFirst(0);

        for (var marble = 1; marble <= maxPoints; marble++)
        {
            if (marble % 23 == 0)
            {
                circle.Rotate(-7);
                var key = marble % players;
                scores.TryAdd(key, 0);
                scores[key] += marble + circle.Last.Value;
                circle.RemoveLast();
            }
            else
            {
                circle.Rotate(2);
                circle.AddLast(marble);
            }
        }

        return scores.Max(s => s.Value);
    }

    protected override object DoPart2(string[] input)
    {
        long players = input[0].ToInt();
        long maxPoints = input[6].ToInt() * 100;

        var scores = new Dictionary<long, long>();
        var circle = new LinkedList<long>();
        circle.AddFirst(0);

        ProgressReporter?.SetSteps(maxPoints);
        for (var marble = 1; marble <= maxPoints; marble++)
        {
            if (marble % 23 == 0)
            {
                circle.Rotate(-7);
                var key = marble % players;
                scores.TryAdd(key, 0);
                scores[key] += marble + circle.Last.Value;
                circle.RemoveLast();
            }
            else
            {
                circle.Rotate(2);
                circle.AddLast(marble);
            }
            
            ProgressReporter?.Step();
        }

        return scores.Max(s => s.Value);
    }

    protected override string[] ParseInputPart1(string input) =>
        input.Split(" ");
}