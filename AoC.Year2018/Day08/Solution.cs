﻿namespace AoC.Year2018.Day08;

public class Solution : Day<int[]>
{
    protected override object DoPart1(int[] input)
    {
        var i = 0;
        var node = Node.GetNode(input, ref i);

        return node.AllSum;
    }

    protected override object DoPart2(int[] input)
    {
        var i = 0;
        var node = Node.GetNode(input, ref i);

        return node.Value;
    }
    
    private class Node
    {
        private List<Node> Nodes { get; set; } = [];
        private List<int> Metadata { get; set; } = [];

        public int AllSum => Metadata.Sum() + Nodes.Sum(x => x.AllSum);
        private int Sum => Metadata.Sum();

        public int Value => Nodes.Count == 0 ? Sum : Metadata.Where(metadata => metadata <= Nodes.Count).Sum(metadata => Nodes[metadata - 1].Value);

        public static Node GetNode(int[] numbers, ref int i)
        {
            var node = new Node();

            var children = numbers[i++];
            var metadata = numbers[i++];

            for (var j = 0; j < children; j++)
                node.Nodes.Add(GetNode(numbers, ref i));

            for (var j = 0; j < metadata; j++)
                node.Metadata.Add(numbers[i++]);

            return node;
        }
    }

    protected override int[] ParseInputPart1(string input) =>
        input.Split(" ").Select(num => num.ToInt()).ToArray();
}