﻿namespace AoC.Year2018.Day01;

public class Solution : Day<int[]>
{
    protected override object DoPart1(int[] input) =>
        input.Sum();

    protected override object DoPart2(int[] input)
    {
        var recentFrequencies = new SortedSet<int> { 0 };
        var frequency = 0;

        while (true)
        {
            foreach (var change in input)
            {
                frequency += change;

                if (!recentFrequencies.Add(frequency))
                    return frequency;
            }
        }
    }

    protected override int[] ParseInputPart1(string input) =>
        input.Split("\n").Select(num => num.ToInt()).ToArray();
}