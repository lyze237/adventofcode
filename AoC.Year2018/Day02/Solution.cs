﻿namespace AoC.Year2018.Day02;

public class Solution : Day<string[]>
{
    protected override object DoPart1(string[] input)
    {
        var twos = 0;
        var threes = 0;

        foreach (var box in input)
        {
            var dictionary = new SortedDictionary<char, int>();

            foreach (var c in box)
            {
                dictionary.TryAdd(c, 0);

                dictionary[c]++;
            }

            var threePairs = dictionary.Where(kvp => kvp.Value == 3).ToList();
            if (threePairs.Count > 0)
                threes++;
            threePairs.ForEach(kvp => dictionary.Remove(kvp.Key));

            var twoPairs = dictionary.Where(kvp => kvp.Value == 2).ToList();
            if (twoPairs.Count > 0)
                twos++;
        }

        return twos * threes;
    }

    protected override object DoPart2(string[] input)
    {
        foreach (var box in input)
        {
            foreach (var compareBox in input)
            {
                var sames = box
                    .Where((c, i) => c == compareBox[i])
                    .Aggregate("", (current, t) => current + t);

                if (sames.Length == box.Length - 1)
                    return sames;
            }
        }

        throw new ArgumentException("No match found");
    }

    protected override string[] ParseInputPart1(string input) =>
        input.Split("\n");
}